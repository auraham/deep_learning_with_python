# cost_sensitive_logistic_regression.py
from collections import Counter
from sklearn.datasets import make_classification
import matplotlib.pyplot as plt
import numpy as np

# define dataset
X, y = make_classification(n_samples=1000,              # number of samples
                           n_features=2,                # number of features
                           n_redundant=0,               # number of redundant features (they are random linear combinations of the informative features)
                           n_classes=2,                 # number of classes
                           n_clusters_per_class=1,      # number of clusters per class
                           weights=[0.99],              # proportions of samples assigned to each class. If None, then the classes are balanced
                           flip_y=0,                    # fraction of samples whose class is assigned randomly
                           random_state=2               # seed
                           )

# summarize class distribution
counter = Counter(y)
print(counter)

# scatter plot
for label, _ in counter.items():
    row_ix = np.where(y == label)[0]
    plt.scatter(X[row_ix, 0], X[row_ix, 1], label=str(label))
plt.legend()
plt.show()