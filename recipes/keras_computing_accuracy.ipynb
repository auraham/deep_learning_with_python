{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Keras: computing accuracy"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Suppose you just finished to train your model and want to evaluate its performance on a large dataset. Here, we show a few ways to compute its accuracy."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## First approach\n",
    "\n",
    "Assumptions:\n",
    "\n",
    "- You have trained your model using an `ImageDataGenerator`.\n",
    "- You include `\"acc\"` in the compilation of the model as follows:\n",
    "\n",
    "```python\n",
    "model.compile(loss=\"binary_crossentropy\",\n",
    "              optimizer=RMSprop(lr=1e-4),\n",
    "              metrics=[\"acc\"]\n",
    "             )\n",
    "```"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Found 1000 images belonging to 2 classes.\n"
     ]
    }
   ],
   "source": [
    "from keras.preprocessing.image import ImageDataGenerator\n",
    "from keras.models import load_model \n",
    "import numpy as np\n",
    "\n",
    "# load model\n",
    "model = load_model(\"../streamlit/cats_and_dogs_small_1.h5\")\n",
    "\n",
    "# create generator\n",
    "test_path = \"/media/data/dogs_vs_cats_small/test\"\n",
    "test_datagen = ImageDataGenerator(rescale=1./255)     # the testing generator is similar to the \n",
    "                                                      # generator employed for training, configure it properly\n",
    "batch_size = 20\n",
    "test_generator = test_datagen.flow_from_directory(\n",
    "                            test_path,\n",
    "                            target_size=(150, 150),\n",
    "                            shuffle=False,            # this is recommended for testing\n",
    "                            batch_size=batch_size,        \n",
    "                            class_mode=\"binary\")\n",
    "\n",
    "# define step size = n_images // batch_size\n",
    "n_images = test_generator.samples\n",
    "steps = n_images // batch_size\n",
    "\n",
    "# compute loss and accuracy\n",
    "test_loss, test_acc = model.evaluate_generator(test_generator, steps)  # chollet, p. 158 "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 10,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "loss: 0.99661190\n",
      "acc:  0.73700000\n"
     ]
    }
   ],
   "source": [
    "print(\"loss: %.8f\" % test_loss)\n",
    "print(\"acc:  %.8f\" % test_acc)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Considerations:\n",
    "- Be sure to setup the generator (`test_generator`) properly. It should use the same configuration of the generator employed for training the model (same `target_size`, `class_mode`, and `rescale`). Attributes like `directory`, `shuffle`, and `batch_size` can be different. See the [documentation](https://faroit.com/keras-docs/1.2.2/preprocessing/image/#imagedatagenerator) for more details.\n",
    "\n",
    "- Here we used `steps = n_images // batch_size` rather than other approaches like `steps = int(np.ceil(n_images/batch_size))`. We followed that method as recommended by [Adrian Rosebrock](https://www.pyimagesearch.com/2018/12/24/how-to-use-keras-fit-and-fit_generator-a-hands-on-tutorial/)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Second approach\n",
    "\n",
    "If you need more control over the batches of images or you do not specified `\"acc\"` when compiling the model, then you can use the following approach."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Found 1000 images belonging to 2 classes.\n",
      "acc:  0.73700000\n"
     ]
    }
   ],
   "source": [
    "from keras.preprocessing.image import ImageDataGenerator\n",
    "from keras.models import load_model \n",
    "import numpy as np\n",
    "\n",
    "# load model\n",
    "model = load_model(\"../streamlit/cats_and_dogs_small_1.h5\")\n",
    "\n",
    "# create generator\n",
    "test_path = \"/media/data/dogs_vs_cats_small/test\"\n",
    "test_datagen = ImageDataGenerator(rescale=1./255)     # the testing generator is similar to the \n",
    "                                                      # generator employed for training, configure it properly\n",
    "batch_size = 20\n",
    "test_generator = test_datagen.flow_from_directory(\n",
    "                            test_path,\n",
    "                            target_size=(150, 150),\n",
    "                            shuffle=False,            # this is recommended for testing\n",
    "                            batch_size=batch_size,        \n",
    "                            class_mode=\"binary\")\n",
    "\n",
    "# define step size = n_images // batch_size\n",
    "n_images = test_generator.samples\n",
    "steps = n_images // batch_size\n",
    "\n",
    "# predict test images\n",
    "pred = model.predict_generator(test_generator, steps)    # [[0.00, 0.99, 0.00, 0.33, ...]]\n",
    "all_pred_labels = (pred > 0.5).astype(\"int32\").flatten()  #  [  0,    1,    0,    0, ...]\n",
    "all_real_labels = test_generator.labels                   #  [  0,    0,    0,    0, ..., 1, 1, 1]\n",
    "\n",
    "# compute accuracy\n",
    "acc = (all_pred_labels == all_real_labels).sum() / len(all_pred_labels)\n",
    "print(\"acc:  %.8f\" % acc)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Considerations:\n",
    "\n",
    "- The output of `model.predict_generator()` are predictions (not labels). As a result, we need to use a threshold for converting the predictions to labels (in this case, it is a binary classification problem, so there are two labels, `0.0` and `1.0`).\n",
    "\n",
    "- We also need to flatten `pred` since its shape is `(1000, 1)`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 22,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "pred: 0.0000, label: 0\n",
      "pred: 0.9998, label: 1\n",
      "pred: 0.0092, label: 0\n",
      "pred: 0.3316, label: 0\n"
     ]
    }
   ],
   "source": [
    "# This block shows a few predictions and their corresponding labels\n",
    "for i in range(4):\n",
    "    print(\"pred: %.4f, label: %d\" % (pred[[i]], all_pred_labels[i]))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "array([0, 0, 0, 0], dtype=int32)"
      ]
     },
     "execution_count": 4,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "# This block shows a few real labels\n",
    "test_generator.labels[:4]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 23,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "(1000, 1)"
      ]
     },
     "execution_count": 23,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "# Shape of the predictions\n",
    "pred.shape"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Third approach\n",
    "\n",
    "Now, we show one more way to compute the accuracy. Here, we predict a batch of images and save the predictions. We repeat this procedure until all the images are processed."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Found 1000 images belonging to 2 classes.\n",
      "batch 10/50\n",
      "batch 20/50\n",
      "batch 30/50\n",
      "batch 40/50\n",
      "batch 50/50\n",
      "acc:  0.73700000\n"
     ]
    }
   ],
   "source": [
    "from keras.preprocessing.image import ImageDataGenerator\n",
    "from keras.models import load_model \n",
    "import numpy as np\n",
    "\n",
    "# load model\n",
    "model = load_model(\"../streamlit/cats_and_dogs_small_1.h5\")\n",
    "\n",
    "# create generator\n",
    "test_path = \"/media/data/dogs_vs_cats_small/test\"\n",
    "test_datagen = ImageDataGenerator(rescale=1./255)     # the testing generator is similar to the \n",
    "                                                      # generator employed for training, configure it properly\n",
    "batch_size = 20\n",
    "test_generator = test_datagen.flow_from_directory(\n",
    "                            test_path,\n",
    "                            target_size=(150, 150),\n",
    "                            shuffle=False,            # this is recommended for testing\n",
    "                            batch_size=batch_size,        \n",
    "                            class_mode=\"binary\")\n",
    "\n",
    "# define step size = n_images // batch_size\n",
    "n_images = test_generator.samples\n",
    "steps = n_images // batch_size\n",
    "\n",
    "# predict test images\n",
    "all_pred_labels = np.zeros(n_images, dtype=\"int32\")\n",
    "all_real_labels = np.zeros(n_images, dtype=\"int32\")\n",
    "\n",
    "for i in range(steps):\n",
    "    \n",
    "    if (i+1)%10 == 0:\n",
    "        print(\"batch %d/%d\" % (i+1, steps))\n",
    "    \n",
    "    for images, true_labels in test_generator:\n",
    "\n",
    "        pred = model.predict(images)\n",
    "        pred_labels = (pred > 0.5).astype('int32').flatten()\n",
    "\n",
    "        # save predictions and real labels\n",
    "        all_pred_labels[i*batch_size:(i+1)*batch_size] = pred_labels\n",
    "        all_real_labels[i*batch_size:(i+1)*batch_size] = true_labels\n",
    "\n",
    "        break\n",
    "\n",
    "# compute accuracy\n",
    "acc = (all_pred_labels == all_real_labels).sum() / len(all_pred_labels)\n",
    "print(\"acc:  %.8f\" % acc)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Considerations:\n",
    "\n",
    "- This approach requires a few more lines of code but it returns the same value for `acc`.\n",
    "- You can also iterate over the batches as follows:\n",
    "\n",
    "\n",
    "```python\n",
    "i = 0\n",
    "for images, true_labels in testn_generator:\n",
    "\n",
    "    pred = model.predict(images)\n",
    "    pred_labels = (pred > 0.5).astype('float').flatten()\n",
    "\n",
    "    # save predictions and real labels\n",
    "    all_pred_labels[i*batch_size:(i+1)*batch_size] = pred_labels\n",
    "    all_real_labels[i*batch_size:(i+1)*batch_size] = true_labels\n",
    "    \n",
    "    # stop criteria\n",
    "    i += 1\n",
    "    if i * batch_size >= n_images:    # chollet p. 147\n",
    "        break\n",
    "        \n",
    "```\n",
    "\n",
    "The full code is given below:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Found 1000 images belonging to 2 classes.\n",
      "batch 10/50\n",
      "batch 20/50\n",
      "batch 30/50\n",
      "batch 40/50\n",
      "batch 50/50\n",
      "acc:  0.73700000\n"
     ]
    }
   ],
   "source": [
    "from keras.preprocessing.image import ImageDataGenerator\n",
    "from keras.models import load_model \n",
    "import numpy as np\n",
    "\n",
    "# load model\n",
    "model = load_model(\"../streamlit/cats_and_dogs_small_1.h5\")\n",
    "\n",
    "# create generator\n",
    "test_path = \"/media/data/dogs_vs_cats_small/test\"\n",
    "test_datagen = ImageDataGenerator(rescale=1./255)     # the testing generator is similar to the \n",
    "                                                      # generator employed for training, configure it properly\n",
    "batch_size = 20\n",
    "test_generator = test_datagen.flow_from_directory(\n",
    "                            test_path,\n",
    "                            target_size=(150, 150),\n",
    "                            shuffle=False,            # this is recommended for testing\n",
    "                            batch_size=batch_size,        \n",
    "                            class_mode=\"binary\")\n",
    "\n",
    "# define step size = n_images // batch_size\n",
    "n_images = test_generator.samples\n",
    "steps = n_images // batch_size\n",
    "\n",
    "# predict test images\n",
    "all_pred_labels = np.zeros(n_images, dtype=\"int32\")\n",
    "all_real_labels = np.zeros(n_images, dtype=\"int32\")\n",
    "\n",
    "i = 0\n",
    "for images, true_labels in test_generator:\n",
    "\n",
    "    pred = model.predict(images)\n",
    "    pred_labels = (pred > 0.5).astype(\"int32\").flatten()\n",
    "\n",
    "    # save predictions and real labels\n",
    "    all_pred_labels[i*batch_size:(i+1)*batch_size] = pred_labels\n",
    "    all_real_labels[i*batch_size:(i+1)*batch_size] = true_labels\n",
    "\n",
    "    if (i+1)%10 == 0:\n",
    "        print(\"batch %d/%d\" % (i+1, steps))\n",
    "    \n",
    "    # stop criteria\n",
    "    i += 1\n",
    "    if i * batch_size >= n_images:    # chollet p. 147\n",
    "        break\n",
    "        \n",
    "# compute accuracy\n",
    "acc = (all_pred_labels == all_real_labels).sum() / len(all_pred_labels)\n",
    "print(\"acc:  %.8f\" % acc)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# References\n",
    "\n",
    "- [Deep Learning with Python]\n",
    "- [pyimagesearch.com](https://www.pyimagesearch.com/2018/12/24/how-to-use-keras-fit-and-fit_generator-a-hands-on-tutorial/)\n",
    "- [Keras documentation](https://faroit.com/keras-docs/1.2.2/preprocessing/image/#imagedatagenerator) \n"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.5.2"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
