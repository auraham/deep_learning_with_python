# server.py
# $ streamlit run server.py
import streamlit as st
import pandas as pd

st.title("Classifier")
st.write("""
## Instructions

1. Upload an image
""")


st.header("Upload image")

uploaded_file = st.file_uploader("Upload an image")
if uploaded_file is not None:
    print(uploaded_file)