# Spark in motion





Imperative programming in Java

```java
int[] array = new int[] {1, 2, 3, 4, 5};
for (int i = 0; i < array.length; i++) {
    array[i] = array[i] * 2;
}
```

Functional programming in Scala

```scala
val array = Array(1, 2, 3, 4, 5)
val doubled = array.map(x => x * 2)
```





## REPL

Declare a function

```
scala> def square(x: Int) = x * x
square: (x: Int)Int
```

Declare a list

```
scala> val list = List(1,2,3,4)
list: List[Int] = List(1, 2, 3, 4)
```

Apply a function to each element of the list:

```scala
scala> list.map(i => i * 2)
res5: List[Int] = List(2, 4, 6, 8)

scala> list.map(i => { i * 2 })
res6: List[Int] = List(2, 4, 6, 8)

scala> list.map(square)
res7: List[Int] = List(1, 4, 9, 16)

scala> list.map {i => i* 2}               // => is called a function literal
res8: List[Int] = List(2, 4, 6, 8)        // similar to a lambda function in python

scala> def doubles(i: Int) = {i * 2}
doubles: (i: Int)Int

scala> list.map(doubles)
res9: List[Int] = List(2, 4, 6, 8) 
```

Assign result to new variable:

```
scala> val newList = list.map(doubles)
newList: List[Int] = List(2, 4, 6, 8)

scala> print(list)
List(1, 2, 3, 4)
scala> print(newList)
List(2, 4, 6, 8)
```

You can use `_` in an expression as a throw-away variable in functions that only takes a single argument:

```scala
scala> list.map( _ * 2)
res15: List[Int] = List(2, 4, 6, 8)
```





## Loading a text file in the REPL

This snippet shows how to read a CSV file and filter lines:

```
scala> val filepath = "file:///media/data/spark_in_motion_data/airport_codes.csv"
filepath: String = file:///media/data/spark_in_motion_data/airport_codes.csv

scala> val lines = sc.textFile(filepath)
lines: org.apache.spark.rdd.RDD[String] = file:///media/data/spark_in_motion_data/airport_codes.csv MapPartitionsRDD[3] at textFile at <console>:26

scala> lines.count
res1: Long = 46236

scala> val filtered = lines.filter {line => line.contains("US-CA")}
filtered: org.apache.spark.rdd.RDD[String] = MapPartitionsRDD[4] at filter at <console>:25

scala> filtered.count
res2: Long = 1016
```

You can even copy and paste that code in a Jupyter notebook.







