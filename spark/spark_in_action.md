# Spark in action





# Java





## Compile first example (Java)

```
git clone https://github.com/jgperrin/net.jgp.books.spark.ch01.git
cd net.jgp.books.spark.ch01
mvn clean install exec:exec
```

Output

```
[INFO] Scanning for projects...
[INFO]                                                                         
[INFO] ------------------------------------------------------------------------
[INFO] Building spark-in-action2-chapter01 1.0.0-SNAPSHOT
[INFO] ------------------------------------------------------------------------
[INFO] 
[INFO] --- maven-clean-plugin:2.5:clean (default-clean) @ spark-in-action2-chapter01 ---
[INFO] Deleting /media/data/spark_in_action/net.jgp.books.spark.ch01/target
[INFO] 
[INFO] --- maven-resources-plugin:2.6:resources (default-resources) @ spark-in-action2-chapter01 ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 1 resource
[INFO] 
[INFO] --- maven-compiler-plugin:3.8.0:compile (default-compile) @ spark-in-action2-chapter01 ---
[INFO] Changes detected - recompiling the module!
[INFO] Compiling 1 source file to /media/data/spark_in_action/net.jgp.books.spark.ch01/target/classes
[INFO] 
[INFO] --- maven-resources-plugin:2.6:testResources (default-testResources) @ spark-in-action2-chapter01 ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] skip non existing resourceDirectory /media/data/spark_in_action/net.jgp.books.spark.ch01/src/test/resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.8.0:testCompile (default-testCompile) @ spark-in-action2-chapter01 ---
[INFO] No sources to compile
[INFO] 
[INFO] --- maven-surefire-plugin:2.17:test (default-test) @ spark-in-action2-chapter01 ---
[INFO] No tests to run.
[INFO] 
[INFO] --- maven-jar-plugin:2.4:jar (default-jar) @ spark-in-action2-chapter01 ---
[INFO] Building jar: /media/data/spark_in_action/net.jgp.books.spark.ch01/target/spark-in-action2-chapter01-1.0.0-SNAPSHOT.jar
[INFO] 
[INFO] --- maven-install-plugin:2.5.2:install (default-install) @ spark-in-action2-chapter01 ---
[INFO] Installing /media/data/spark_in_action/net.jgp.books.spark.ch01/target/spark-in-action2-chapter01-1.0.0-SNAPSHOT.jar to /home/auraham/.m2/repository/net/jgp/books/spark-in-action2-chapter01/1.0.0-SNAPSHOT/spark-in-action2-chapter01-1.0.0-SNAPSHOT.jar
[INFO] Installing /media/data/spark_in_action/net.jgp.books.spark.ch01/pom.xml to /home/auraham/.m2/repository/net/jgp/books/spark-in-action2-chapter01/1.0.0-SNAPSHOT/spark-in-action2-chapter01-1.0.0-SNAPSHOT.pom
[INFO] 
[INFO] --- exec-maven-plugin:1.6.0:exec (default-cli) @ spark-in-action2-chapter01 ---
+---+--------+--------------------+-----------+--------------------+
| id|authorId|               title|releaseDate|                link|
+---+--------+--------------------+-----------+--------------------+
|  1|       1|Fantastic Beasts ...|   11/18/16|http://amzn.to/2k...|
|  2|       1|Harry Potter and ...|    10/6/15|http://amzn.to/2l...|
|  3|       1|The Tales of Beed...|    12/4/08|http://amzn.to/2k...|
|  4|       1|Harry Potter and ...|    10/4/16|http://amzn.to/2k...|
|  5|       2|Informix 12.10 on...|    4/23/17|http://amzn.to/2i...|
+---+--------+--------------------+-----------+--------------------+
only showing top 5 rows

[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time: 11.571 s
[INFO] Finished at: 2021-02-17T22:51:48-06:00
[INFO] Final Memory: 38M/318M
[INFO] ------------------------------------------------------------------------
```





## Compile second example (Java)


```
git clone https://github.com/jgperrin/net.jgp.books.spark.ch02.git
cd net.jgp.books.spark.ch02
mvn clean install exec:exec
```

Output:

```
[INFO] Scanning for projects...
[INFO]                                                                         
[INFO] ------------------------------------------------------------------------
[INFO] Building spark-in-action2-chapter02 1.0.0-SNAPSHOT
[INFO] ------------------------------------------------------------------------
[INFO] 
[INFO] --- maven-clean-plugin:2.5:clean (default-clean) @ spark-in-action2-chapter02 ---
[INFO] Deleting /media/data/spark_in_action/net.jgp.books.spark.ch02/target
[INFO] 
[INFO] --- maven-resources-plugin:2.6:resources (default-resources) @ spark-in-action2-chapter02 ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 1 resource
[INFO] 
[INFO] --- maven-compiler-plugin:3.8.0:compile (default-compile) @ spark-in-action2-chapter02 ---
[INFO] Changes detected - recompiling the module!
[INFO] Compiling 5 source files to /media/data/spark_in_action/net.jgp.books.spark.ch02/target/classes
[INFO] 
[INFO] --- maven-resources-plugin:2.6:testResources (default-testResources) @ spark-in-action2-chapter02 ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] skip non existing resourceDirectory /media/data/spark_in_action/net.jgp.books.spark.ch02/src/test/resources
[INFO] 
[INFO] --- maven-compiler-plugin:3.8.0:testCompile (default-testCompile) @ spark-in-action2-chapter02 ---
[INFO] No sources to compile
[INFO] 
[INFO] --- maven-surefire-plugin:2.17:test (default-test) @ spark-in-action2-chapter02 ---
[INFO] No tests to run.
[INFO] 
[INFO] --- maven-jar-plugin:2.4:jar (default-jar) @ spark-in-action2-chapter02 ---
[INFO] Building jar: /media/data/spark_in_action/net.jgp.books.spark.ch02/target/spark-in-action2-chapter02-1.0.0-SNAPSHOT.jar
[INFO] 
[INFO] --- maven-install-plugin:2.5.2:install (default-install) @ spark-in-action2-chapter02 ---
[INFO] Installing /media/data/spark_in_action/net.jgp.books.spark.ch02/target/spark-in-action2-chapter02-1.0.0-SNAPSHOT.jar to /home/auraham/.m2/repository/net/jgp/books/spark-in-action2-chapter02/1.0.0-SNAPSHOT/spark-in-action2-chapter02-1.0.0-SNAPSHOT.jar
[INFO] Installing /media/data/spark_in_action/net.jgp.books.spark.ch02/pom.xml to /home/auraham/.m2/repository/net/jgp/books/spark-in-action2-chapter02/1.0.0-SNAPSHOT/spark-in-action2-chapter02-1.0.0-SNAPSHOT.pom
[INFO] 
[INFO] --- exec-maven-plugin:1.6.0:exec (default-cli) @ spark-in-action2-chapter02 ---
Error: Could not find or load main class net.jgp.books.spark.ch02.CsvToRelationalDatabaseApp
[ERROR] Command execution failed.
org.apache.commons.exec.ExecuteException: Process exited with an error: 1 (Exit value: 1)
	at org.apache.commons.exec.DefaultExecutor.executeInternal(DefaultExecutor.java:404)
	at org.apache.commons.exec.DefaultExecutor.execute(DefaultExecutor.java:166)
	at org.codehaus.mojo.exec.ExecMojo.executeCommandLine(ExecMojo.java:804)
	at org.codehaus.mojo.exec.ExecMojo.executeCommandLine(ExecMojo.java:751)
	at org.codehaus.mojo.exec.ExecMojo.execute(ExecMojo.java:313)
	at org.apache.maven.plugin.DefaultBuildPluginManager.executeMojo(DefaultBuildPluginManager.java:134)
	at org.apache.maven.lifecycle.internal.MojoExecutor.execute(MojoExecutor.java:207)
	at org.apache.maven.lifecycle.internal.MojoExecutor.execute(MojoExecutor.java:153)
	at org.apache.maven.lifecycle.internal.MojoExecutor.execute(MojoExecutor.java:145)
	at org.apache.maven.lifecycle.internal.LifecycleModuleBuilder.buildProject(LifecycleModuleBuilder.java:116)
	at org.apache.maven.lifecycle.internal.LifecycleModuleBuilder.buildProject(LifecycleModuleBuilder.java:80)
	at org.apache.maven.lifecycle.internal.builder.singlethreaded.SingleThreadedBuilder.build(SingleThreadedBuilder.java:51)
	at org.apache.maven.lifecycle.internal.LifecycleStarter.execute(LifecycleStarter.java:128)
	at org.apache.maven.DefaultMaven.doExecute(DefaultMaven.java:307)
	at org.apache.maven.DefaultMaven.doExecute(DefaultMaven.java:193)
	at org.apache.maven.DefaultMaven.execute(DefaultMaven.java:106)
	at org.apache.maven.cli.MavenCli.execute(MavenCli.java:863)
	at org.apache.maven.cli.MavenCli.doMain(MavenCli.java:288)
	at org.apache.maven.cli.MavenCli.main(MavenCli.java:199)
	at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)
	at sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:62)
	at sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)
	at java.lang.reflect.Method.invoke(Method.java:498)
	at org.codehaus.plexus.classworlds.launcher.Launcher.launchEnhanced(Launcher.java:289)
	at org.codehaus.plexus.classworlds.launcher.Launcher.launch(Launcher.java:229)
	at org.codehaus.plexus.classworlds.launcher.Launcher.mainWithExitCode(Launcher.java:415)
	at org.codehaus.plexus.classworlds.launcher.Launcher.main(Launcher.java:356)
[INFO] ------------------------------------------------------------------------
[INFO] BUILD FAILURE
[INFO] ------------------------------------------------------------------------
[INFO] Total time: 3.363 s
[INFO] Finished at: 2021-02-17T22:54:31-06:00
[INFO] Final Memory: 45M/384M
[INFO] ------------------------------------------------------------------------
[ERROR] Failed to execute goal org.codehaus.mojo:exec-maven-plugin:1.6.0:exec (default-cli) on project spark-in-action2-chapter02: Command execution failed. Process exited with an error: 1 (Exit value: 1) -> [Help 1]
[ERROR] 
[ERROR] To see the full stack trace of the errors, re-run Maven with the -e switch.
[ERROR] Re-run Maven using the -X switch to enable full debug logging.
[ERROR] 
[ERROR] For more information about the errors and possible solutions, please read the following articles:
[ERROR] [Help 1] http://cwiki.apache.org/confluence/display/MAVEN/MojoExecutionException
```



According to this page:

https://cwiki.apache.org/confluence/display/MAVEN/MojoExecutionException

> Unlike many other errors, this exception is not generated by the Maven core itself but by a plugin. As a rule of thumb, plugins use this error to signal a problem in their
> configuration or the information they retrieved from the POM.



```
$ cd net.jgp.books.spark.ch01
$ colordiff pom.xml ../net.jgp.books.spark.ch02/pom.xml
```

```
7c7
<   <artifactId>spark-in-action2-chapter01</artifactId>
---
>   <artifactId>spark-in-action2-chapter02</artifactId>
11d10
< 
16a16,17
>     <postgresql.version>42.1.4</postgresql.version>
>     <derby.version>10.14.2.0</derby.version>
55a57,81
> 
>     <dependency>
>       <groupId>org.postgresql</groupId>
>       <artifactId>postgresql</artifactId>
>       <version>${postgresql.version}</version>
>     </dependency>
> 
>     <dependency>
>       <groupId>org.apache.derby</groupId>
>       <artifactId>derby</artifactId>
>       <version>${derby.version}</version>
>     </dependency>
>     
>     <dependency>
>       <groupId>org.apache.derby</groupId>
>       <artifactId>derbynet</artifactId>
>       <version>${derby.version}</version>
>     </dependency>
> 
>     <dependency>
>       <groupId>org.apache.derby</groupId>
>       <artifactId>derbyclient</artifactId>
>       <version>${derby.version}</version>
>     </dependency>
> 
61a88
> 
81a109
>             <argument>-Dlog4j.configuration=file:src/main/java/log4j.properties</argument>
84c112
<             <argument>net.jgp.books.spark.ch01.lab100_csv_to_dataframe.CsvToDataframeApp</argument>
---
>             <argument>net.jgp.books.spark.ch02.CsvToRelationalDatabaseApp</argument>
89a118
> 
```

There are two dependencies in `net.jgp.books.spark.ch02` that are not in `net.jgp.books.spark.ch01`:

```
>     <postgresql.version>42.1.4</postgresql.version>
>     <derby.version>10.14.2.0</derby.version>
```

So, I think I must install them before running `mvn clean install exec:exec` again.



**First try: Remove occurrences of Derby and Postgres**

I first removed the dependencies from `pom.xml`, specifically:

```xml
<!--postgresql.version>42.1.4</postgresql.version>
    <derby.version>10.14.2.0</derby.version-->

 <!--dependency>
      <groupId>org.postgresql</groupId>
      <artifactId>postgresql</artifactId>
      <version>${postgresql.version}</version>
    </dependency>

    <dependency>
      <groupId>org.apache.derby</groupId>
      <artifactId>derby</artifactId>
      <version>${derby.version}</version>
    </dependency>
    
    <dependency>
      <groupId>org.apache.derby</groupId>
      <artifactId>derbynet</artifactId>
      <version>${derby.version}</version>
    </dependency>

    <dependency>
      <groupId>org.apache.derby</groupId>
      <artifactId>derbyclient</artifactId>
      <version>${derby.version}</version>
    </dependency-->
```

Then, I noticed that I must change the path of the main class as follows (you need to add `lab100_csv_to_db`):

```xml
<argument>net.jgp.books.spark.ch02.lab100_csv_to_db.CsvToRelationalDatabaseApp</argument>
```

This is because the second lab contains three *main* files. Thus, we need to specify which one is the main file in `pom.xml`. 

Then, I commented this block in `CsvToRelationalDatabaseApp.java` since I do not have Postgres:

```java
// Properties to connect to the database, the JDBC driver is part of our
// pom.xml
Properties prop = new Properties();
prop.setProperty("driver", "org.postgresql.Driver");
prop.setProperty("user", "jgp");
prop.setProperty("password", "Spark<3Java");

// Write in a table called ch02
//df.write()
//    .mode(SaveMode.Overwrite)
//    .jdbc(dbConnectionUrl, "ch02", prop);
```

Instead, we will print some rows of the dataframe:

```java
// debug
df.show();
```

Now, we can run the project with maven:

```
$ net.jgp.books.spark.ch02
$ mvn clean install exec:exec
```

This is part of the output:

```
21/02/17 23:21:04 INFO CodeGenerator: Code generated in 52.481033 ms
+--------+--------------+--------------------+
|   lname|         fname|                name|
+--------+--------------+--------------------+
|  Pascal|        Blaise|      Pascal, Blaise|
|Voltaire|      François|  Voltaire, François|
|  Perrin|  Jean-Georges|Perrin, Jean-Georges|
|Maréchal|Pierre Sylvain|Maréchal, Pierre ...|
|   Karau|        Holden|       Karau, Holden|
| Zaharia|         Matei|      Zaharia, Matei|
+--------+--------------+--------------------+

Process complete
```

I noticed that there is one error:

```
log4j:ERROR Could not read configuration file from URL [file:src/main/java/log4j.properties].
java.io.FileNotFoundException: src/main/java/log4j.properties (No such file or directory)
	at java.io.FileInputStream.open0(Native Method)
	at java.io.FileInputStream.open(FileInputStream.java:195)
	at java.io.FileInputStream.<init>(FileInputStream.java:138)
	at java.io.FileInputStream.<init>(FileInputStream.java:93)
	at sun.net.www.protocol.file.FileURLConnection.connect(FileURLConnection.java:90)
	at sun.net.www.protocol.file.FileURLConnection.getInputStream(FileURLConnection.java:188)
	at org.apache.log4j.PropertyConfigurator.doConfigure(PropertyConfigurator.java:557)
	at org.apache.log4j.helpers.OptionConverter.selectAndConfigure(OptionConverter.java:526)
	at org.apache.log4j.LogManager.<clinit>(LogManager.java:127)
	at org.slf4j.impl.Log4jLoggerFactory.<init>(Log4jLoggerFactory.java:66)
	at org.slf4j.impl.StaticLoggerBinder.<init>(StaticLoggerBinder.java:72)
	at org.slf4j.impl.StaticLoggerBinder.<clinit>(StaticLoggerBinder.java:45)
	at org.slf4j.LoggerFactory.bind(LoggerFactory.java:150)
	at org.slf4j.LoggerFactory.performInitialization(LoggerFactory.java:124)
	at org.slf4j.LoggerFactory.getILoggerFactory(LoggerFactory.java:417)
	at org.slf4j.LoggerFactory.getLogger(LoggerFactory.java:362)
	at org.slf4j.LoggerFactory.getLogger(LoggerFactory.java:388)
	at org.apache.spark.network.util.JavaUtils.<clinit>(JavaUtils.java:41)
	at org.apache.spark.internal.config.ConfigHelpers$.byteFromString(ConfigBuilder.scala:67)
	at org.apache.spark.internal.config.ConfigBuilder.$anonfun$bytesConf$1(ConfigBuilder.scala:259)
	at org.apache.spark.internal.config.ConfigBuilder.$anonfun$bytesConf$1$adapted(ConfigBuilder.scala:259)
	at org.apache.spark.internal.config.TypedConfigBuilder.$anonfun$transform$1(ConfigBuilder.scala:101)
	at org.apache.spark.internal.config.TypedConfigBuilder.createWithDefault(ConfigBuilder.scala:144)
	at org.apache.spark.internal.config.package$.<init>(package.scala:337)
	at org.apache.spark.internal.config.package$.<clinit>(package.scala)
	at org.apache.spark.SparkConf$.<init>(SparkConf.scala:639)
	at org.apache.spark.SparkConf$.<clinit>(SparkConf.scala)
	at org.apache.spark.SparkConf.set(SparkConf.scala:94)
	at org.apache.spark.SparkConf.set(SparkConf.scala:83)
	at org.apache.spark.sql.SparkSession$Builder.$anonfun$getOrCreate$2(SparkSession.scala:923)
	at scala.collection.mutable.HashMap.$anonfun$foreach$1(HashMap.scala:149)
	at scala.collection.mutable.HashTable.foreachEntry(HashTable.scala:237)
	at scala.collection.mutable.HashTable.foreachEntry$(HashTable.scala:230)
	at scala.collection.mutable.HashMap.foreachEntry(HashMap.scala:44)
	at scala.collection.mutable.HashMap.foreach(HashMap.scala:149)
	at org.apache.spark.sql.SparkSession$Builder.$anonfun$getOrCreate$1(SparkSession.scala:923)
	at scala.Option.getOrElse(Option.scala:189)
	at org.apache.spark.sql.SparkSession$Builder.getOrCreate(SparkSession.scala:921)
	at net.jgp.books.spark.ch02.lab100_csv_to_db.CsvToRelationalDatabaseApp.start(CsvToRelationalDatabaseApp.java:39)
	at net.jgp.books.spark.ch02.lab100_csv_to_db.CsvToRelationalDatabaseApp.main(CsvToRelationalDatabaseApp.java:28)
log4j:ERROR Ignoring configuration file [file:src/main/java/log4j.properties].
Using Spark's default log4j profile: org/apache/spark/log4j-defaults.properties
```

So, we may want to provide a `log4j.properties` file in `src/main/java`. In case you are wondering, that path is defined in `pom.xml`:

```
<argument>-Dlog4j.configuration=file:src/main/java/log4j.properties</argument>
```

According to this [post](https://stackoverflow.com/questions/30924710/how-to-get-rid-of-using-sparks-default-log4j-profile-org-apache-spark-log4j-d), you can provide it as follows:

```
$ media/data/spark_in_action/net.jgp.books.spark.ch02/src/main/java
$ cp /media/data/spark/conf/log4j.properties.template log4j.properties
```

Here, `$SPARK_HOME` is `/media/data/spark`

After adding that file, the error message will be fixed.



**Second try: install postgres**



After installing postgres, the user and the database for the lab of the chapter 2, you can update `pom.xml` to enable the Postgres dependency only (Apache Derby is unabled):

```xml
<postgresql.version>42.1.4</postgresql.version>  <!-- creo que la version 42.1 se refiere a la del connector, no a la version de postgres instalada en el sistema-->

<dependency>
      <groupId>org.postgresql</groupId>
      <artifactId>postgresql</artifactId>
      <version>${postgresql.version}</version>
    </dependency>
```

Then, update the main script:

```java
// Properties to connect to the database, the JDBC driver is part of our
    // pom.xml
    Properties prop = new Properties();
    prop.setProperty("driver", "org.postgresql.Driver");
    prop.setProperty("user", "auraham");
    prop.setProperty("password", "spark");

    // Write in a table called ch02
    df.write()
        .mode(SaveMode.Overwrite)
        .jdbc(dbConnectionUrl, "ch02", prop);
```

And that's it!





# Scala



## Compile first example

After installing scala and sbt, run these commands:

```
$ cd media/data/spark_in_action/net.jgp.books.spark.ch01
$ sbt clean assembly
```

That command will download a lot of dependencies (`jar` files). This is the last part of the output:

```
[info] Done updating.
[warn] Found version conflict(s) in library dependencies; some are suspected to be binary incompatible:
[warn] 	* com.google.code.findbugs:jsr305:3.0.2 is selected over 1.3.9
[warn] 	    +- org.apache.arrow:arrow-vector:0.10.0               (depends on 3.0.2)
[warn] 	    +- org.apache.arrow:arrow-memory:0.10.0               (depends on 3.0.2)
[warn] 	    +- org.apache.spark:spark-unsafe_2.11:2.4.4           (depends on 1.3.9)
[warn] 	    +- com.google.guava:guava:11.0.2                      (depends on 1.3.9)
[warn] 	    +- org.apache.spark:spark-core_2.11:2.4.4             (depends on 1.3.9)
[warn] 	    +- org.apache.spark:spark-network-common_2.11:2.4.4   (depends on 1.3.9)
[warn] 	    +- org.apache.hadoop:hadoop-common:2.6.5              (depends on 1.3.9)
[warn] 	* io.netty:netty:3.9.9.Final is selected over {3.6.2.Final, 3.7.0.Final}
[warn] 	    +- org.apache.spark:spark-core_2.11:2.4.4             (depends on 3.9.9.Final)
[warn] 	    +- org.apache.zookeeper:zookeeper:3.4.6               (depends on 3.6.2.Final)
[warn] 	    +- org.apache.hadoop:hadoop-hdfs:2.6.5                (depends on 3.6.2.Final)
[warn] 	* com.google.guava:guava:11.0.2 is selected over {12.0.1, 16.0.1}
[warn] 	    +- org.apache.hadoop:hadoop-yarn-client:2.6.5         (depends on 11.0.2)
[warn] 	    +- org.apache.hadoop:hadoop-yarn-api:2.6.5            (depends on 11.0.2)
[warn] 	    +- org.apache.hadoop:hadoop-yarn-common:2.6.5         (depends on 11.0.2)
[warn] 	    +- org.apache.hadoop:hadoop-yarn-server-nodemanager:2.6.5 (depends on 11.0.2)
[warn] 	    +- org.apache.hadoop:hadoop-yarn-server-common:2.6.5  (depends on 11.0.2)
[warn] 	    +- org.apache.hadoop:hadoop-hdfs:2.6.5                (depends on 11.0.2)
[warn] 	    +- org.apache.curator:curator-framework:2.6.0         (depends on 16.0.1)
[warn] 	    +- org.apache.curator:curator-client:2.6.0            (depends on 16.0.1)
[warn] 	    +- org.apache.curator:curator-recipes:2.6.0           (depends on 16.0.1)
[warn] 	    +- org.apache.hadoop:hadoop-common:2.6.5              (depends on 16.0.1)
[warn] 	    +- org.htrace:htrace-core:3.0.4                       (depends on 12.0.1)
[warn] Run 'evicted' to see detailed eviction warnings
[info] Compiling 1 Scala source and 1 Java source to /media/data/spark_in_action/net.jgp.books.spark.ch01/target/scala-2.11/classes ...
[info] Non-compiled module 'compiler-bridge_2.11' for Scala 2.11.11. Compiling...
[info]   Compilation completed in 11.271s.
[info] Done compiling.
[warn] Multiple main classes detected.  Run 'show discoveredMainClasses' to see the list
[info] Strategy 'discard' was applied to 327 files (Run the task at debug level to see details)
[info] Strategy 'first' was applied to 383 files (Run the task at debug level to see details)
[info] Packaging /media/data/spark_in_action/net.jgp.books.spark.ch01/target/scala-2.11/SparkInAction2-Chapter01-assembly-1.0.0.jar ...
[info] Done packaging.
[success] Total time: 517 s, completed Feb 18, 2021 9:32:09 AM
```

It took almost 8 min to complete. Then, run this command as explained in README.md:

```
$ spark-submit --class net.jgp.books.spark.ch01.lab100_csv_to_dataframe.CsvToDataframeScalaApp target/scala-2.11/SparkInAction2-Chapter01-assembly-1.0.0.jar
```

This is part of the output:

```
21/02/18 09:42:41 INFO DAGScheduler: Job 1 finished: show at CsvToDataframeScalaApp.scala:31, took 0.067399 s
+---+--------+--------------------+-----------+--------------------+
| id|authorId|               title|releaseDate|                link|
+---+--------+--------------------+-----------+--------------------+
|  1|       1|Fantastic Beasts ...|   11/18/16|http://amzn.to/2k...|
|  2|       1|Harry Potter and ...|    10/6/15|http://amzn.to/2l...|
|  3|       1|The Tales of Beed...|    12/4/08|http://amzn.to/2k...|
|  4|       1|Harry Potter and ...|    10/4/16|http://amzn.to/2k...|
|  5|       2|Informix 12.10 on...|    4/23/17|http://amzn.to/2i...|
+---+--------+--------------------+-----------+--------------------+
only showing top 5 rows
```



## Compile second example

Be sure to update your credentials:

```scala
// debug
df.show(5)

// Step 3: Save
// ----
// The connection URL, assuming your PostgreSQL instance runs locally on
// the
// default port, and the database we use is "spark_labs"
val dbConnectionUrl = "jdbc:postgresql://localhost/spark_labs"

// Properties to connect to the database, the JDBC driver is part of our
// pom.xml
val prop = new Properties
prop.setProperty("driver", "org.postgresql.Driver")
prop.setProperty("user", "auraham")
prop.setProperty("password", "spark")
```

Now, run these commands:

```
$ cd media/data/spark_in_action/net.jgp.books.spark.ch02
$ sbt clean assembly
```

Again, it will take a few minutes and download a lot of dependencies. This is the last part of the output:

```
[info] Done updating.
[warn] Found version conflict(s) in library dependencies; some are suspected to be binary incompatible:
[warn] 	* io.netty:netty-all:4.1.42.Final is selected over 4.0.23.Final
[warn] 	    +- org.apache.spark:spark-core_2.12:3.0.0-preview2    (depends on 4.1.42.Final)
[warn] 	    +- org.apache.spark:spark-network-common_2.12:3.0.0-preview2 (depends on 4.1.42.Final)
[warn] 	    +- org.apache.hadoop:hadoop-hdfs:2.7.4                (depends on 4.0.23.Final)
[warn] 	* com.google.code.findbugs:jsr305:3.0.0 is selected over {3.0.2, 1.3.9}
[warn] 	    +- org.apache.spark:spark-core_2.12:3.0.0-preview2    (depends on 3.0.0)
[warn] 	    +- org.apache.spark:spark-unsafe_2.12:3.0.0-preview2  (depends on 3.0.0)
[warn] 	    +- org.apache.hadoop:hadoop-common:2.7.4              (depends on 3.0.0)
[warn] 	    +- org.apache.spark:spark-network-common_2.12:3.0.0-preview2 (depends on 3.0.0)
[warn] 	    +- com.google.guava:guava:11.0.2                      (depends on 1.3.9)
[warn] 	    +- org.apache.arrow:arrow-memory:0.15.1               (depends on 3.0.2)
[warn] 	    +- com.github.spotbugs:spotbugs-annotations:3.1.9     (depends on 3.0.2)
[warn] 	* com.google.guava:guava:11.0.2 is selected over 16.0.1
[warn] 	    +- org.apache.hadoop:hadoop-yarn-server-nodemanager:2.7.4 (depends on 11.0.2)
[warn] 	    +- org.apache.hadoop:hadoop-yarn-api:2.7.4            (depends on 11.0.2)
[warn] 	    +- org.apache.hadoop:hadoop-yarn-common:2.7.4         (depends on 11.0.2)
[warn] 	    +- org.apache.hadoop:hadoop-hdfs:2.7.4                (depends on 11.0.2)
[warn] 	    +- org.apache.hadoop:hadoop-yarn-client:2.7.4         (depends on 11.0.2)
[warn] 	    +- org.apache.hadoop:hadoop-yarn-server-common:2.7.4  (depends on 11.0.2)
[warn] 	    +- org.apache.curator:curator-framework:2.7.1         (depends on 16.0.1)
[warn] 	    +- org.apache.curator:curator-client:2.7.1            (depends on 16.0.1)
[warn] 	    +- org.apache.hadoop:hadoop-common:2.7.4              (depends on 16.0.1)
[warn] 	    +- org.apache.curator:curator-recipes:2.7.1           (depends on 16.0.1)
[warn] Run 'evicted' to see detailed eviction warnings
[info] Compiling 1 Scala source and 3 Java sources to /media/data/spark_in_action/net.jgp.books.spark.ch02/target/scala-2.12/classes ...
[info] Non-compiled module 'compiler-bridge_2.12' for Scala 2.12.10. Compiling...
[info]   Compilation completed in 11.388s.
[info] Done compiling.
[warn] Multiple main classes detected.  Run 'show discoveredMainClasses' to see the list
[info] Strategy 'discard' was applied to 337 files (Run the task at debug level to see details)
[info] Strategy 'first' was applied to 30 files (Run the task at debug level to see details)
[info] Packaging /media/data/spark_in_action/net.jgp.books.spark.ch02/target/scala-2.12/SparkInAction2-Chapter02-assembly-1.0.0.jar ...
[info] Done packaging.
[success] Total time: 319 s, completed Feb 18, 2021 9:57:04 AM
```

Finally, submit the application:

```
$ spark-submit --class net.jgp.books.spark.ch02.lab100_csv_to_db.CsvToRelationalDatabaseScalaApp target/scala-2.12/SparkInAction2-Chapter02-assembly-1.0.0.jar
```

This is part of the output:

```
+--------+--------------+--------------------+
|   lname|         fname|                name|
+--------+--------------+--------------------+
|  Pascal|        Blaise|      Pascal, Blaise|
|Voltaire|      François|  Voltaire, François|
|  Perrin|  Jean-Georges|Perrin, Jean-Georges|
|Maréchal|Pierre Sylvain|Maréchal, Pierre ...|
|   Karau|        Holden|       Karau, Holden|
+--------+--------------+--------------------+
only showing top 5 rows
```

Running an application with sbt (scala) takes more time than with maven (java), at least considering these two examples. Also, sbt download a lot of dependencies: even when the example two is rather similar to the first example, sbt downloaded a large number of dependencies. The main difference between both examples is the database connection. Also, sbt throws a lot of info messages:

```
[warn] Multiple main classes detected.  Run 'show discoveredMainClasses' to see the list
[info] Strategy 'discard' was applied to 337 files (Run the task at debug level to see details)
[info] Strategy 'first' was applied to 30 files (Run the task at debug level to see details)
[info] Packaging /media/data/spark_in_action/net.jgp.books.spark.ch02/target/scala-2.12/SparkInAction2-Chapter02-assembly-1.0.0.jar ...
[info] Done packaging.
[success] Total time: 319 s, completed Feb 18, 2021 9:57:04 AM
```

What are those strategies and why were these strategies applied? The second lab only contains a few files. However, I do not know why a 'discard' strategy was applied to 337 files. 