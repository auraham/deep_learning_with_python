# Roadmap

Mi objetivo es aprender pytorch, no hacer una comparacion entre pytorch y keras (aunque puede ser interesante)

- [x] Probar el codigo de nielsen en theano para programar mi propia convolucion con numpy
- [ ] Usar las notas del extended chapter 6 para entender la convolucion en 3 canales
- [ ] Leer de nuevo el cap del libro de rosenbrock para programar una NN desde scratch
- [ ] Leer el post que menciona rosenbrock con los calculos a mano de backprop
- [ ] Leer este post para hacer la NN con pytorch https://jhui.github.io/2018/02/09/PyTorch-neural-networks/







