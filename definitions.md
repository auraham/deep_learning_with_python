# Definitions



## Dimensionality, axes, and ranks



> The *size* (or *shape*, in Numpy parlance) is a tuple indicating how many elements across each *dimension* the tensor represents. [Stevens]



> The number of axes of a tensor is also called its *rank*. You can display the number of axes of a Numpy tensor via the `ndim` attribute. [Chollet]

Then, the author gives this example:

```python
>>> x = np.array([12, 3, 6, 14, 7])
>>> x.ndim
1                 # number of axes or rank
```

> This vector has five entries and so is called a 5-dimensional vector. Don't confuse a 5D vector with a 5D tensor! A 5D vector has only one axis and has five dimensions along its axis, whereas a 5D tensor has five axes







## References

- [Stevens] *Deep Learning with Pytorch*
- [Chollet]