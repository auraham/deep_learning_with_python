

- The image is the class
- The container is the instance of the class

https://blog.sixeyed.com/learn-docker-in-one-month-your-week-1-guide/



Este ejemplo es un hola mundo

```
sudo docker container run diamol/ch02-hello-diamol
```

```
---------------------
Hello from Chapter 2!
---------------------
My name is:
963108945b5f
---------------------
Im running on:
Linux 4.15.0-133-generic x86_64
---------------------
My address is:
inet addr:172.17.0.2 Bcast:172.17.255.255 Mask:255.255.0.0
---------------------
```



Este comando cambia mi shell por el shell del container. Es como hacer un ssh al container en un solo comando. It happens to be a container but it may be a remote machine.

```
sudo docker container run --interactive --tty diamol/base
```

```
Unable to find image 'diamol/base:latest' locally
latest: Pulling from diamol/base
31603596830f: Already exists 
Digest: sha256:8a0a4e35241af4f4b16138850c88e4b7328f4a36eee0769e58e13812c7d02adc
Status: Downloaded newer image for diamol/base:latest
/ # ls
bin    dev    etc    home   lib    media  mnt    opt    proc   root   run    sbin   srv    sys    tmp    usr    var
/ # 
```







Por que no puedo hacer esto, una sesion interactiva con esa imagen?

```
sudo docker container run --interactive --tty diamol/ch02-hello-diamol
```

The container is just a wrapper around an application: when the appliation is done, the container finishes (so the container stops using any compute power).





```
 [python37] auraham@rocket ~ $ sudo docker cp sampleee dc4:/home/

```



```
 [python37] auraham@rocket ~ $ sudo docker exec -it 07e /bin/bash
OCI runtime exec failed: exec failed: container_linux.go:345: starting container process caused "exec: \"/bin/bash\": stat /bin/bash: no such file or directory": unknown
```

