# Notes



## Use Jupyter notebook remotely

**Server**

```
jupyter notebook --no-browser --port=8889
```

Alternatively:

```
nohup jupyter notebook --no-browser --port=8889 &
```

**Local computer**

Redirect ports:

```
ssh -N -f -L localhost:8888:localhost:8889 username@your_remote_host_name
```

Open this address in your browser:

```
localhost:8888
```

[Use Jupyter notebook remotely](https://amber-md.github.io/pytraj/latest/tutorials/remote_jupyter_notebook)
[Running a Jupyter notebook from a remote server](https://ljvmiranda921.github.io/notebook/2018/01/31/running-a-jupyter-notebook/)






## Displaying images


Consider this snippet:

```python
# test_model.py
from __future__ import print_function
import numpy as np
import matplotlib.pyplot as plt
from keras.preprocessing import image
import os

if __name__ == "__main__":
    
    test_dir_dogs = "/media/data/dogs_vs_cats_small/test/dogs"
    fnames = [os.path.join(test_dir_dogs, fname) for fname in os.listdir(test_dir_dogs)]

    img_path = fnames[4]
    img = image.load_img(img_path)
    x = image.img_to_array(img)
    
    plt.imshow(x)
```

You will get this error when trying to display an image:

```
W1007 12:11:58.658936 140436321801984 image.py:648] Clipping input data to the valid range for imshow with RGB data ([0..1] for floats or [0..255] for integers).
```

That is because the `dtype` of the image `x` is not correct. In this case, the image is converted from `PIL` to `np.array` as follows:

```python
# incorrect casting
img = image.load_img(img_path)
x = image.img_to_array(img)
```

However, if we inspect `x`, we notice that it is a `float32` array but its range is not [0, 1]:

```
In [34]: x.dtype
Out[34]: dtype('float32')

In [35]: x[:4, :4, 0]        
Out[35]: 
array([[225., 226., 229., 232.],
       [215., 216., 217., 219.],
       [209., 209., 208., 208.],
       [212., 211., 209., 208.]], dtype=float32)
```

From the [documentation](https://matplotlib.org/3.1.1/api/_as_gen/matplotlib.pyplot.imshow.html), `plt.imshow` expects one of these options:

- `(M, N)`: an image with scalar data.
- `(M, N, 3)`: an image with RGB values **(0-1 float or 0-255 int)**.
- `(M, N, 4)`: an image with RGBA values (0-1 float or 0-255 int), including transparency.

Our image is does not fit on any of these options. We can either change the type or the range (via normalization).  Let's check both methods:

```python
# convert from PIL to np.array (cast to np.int in the range [0, 255])
img_path = fnames[4]
img = image.load_img(img_path)
x = image.img_to_array(img).astype(int)
plt.figure()
plt.imshow(x)
```

```python
# convert rfrom PIL to np.array (as np.float32 in the range [0, 1])
img_path = fnames[5]
img = image.load_img(img_path)
x = image.img_to_array(img) * 1./255
plt.figure()
plt.imshow(x)
```

Just for completeness, here is the full code:

```python
# test_model.py
from __future__ import print_function
import numpy as np
import matplotlib.pyplot as plt
from keras.preprocessing import image
import os

if __name__ == "__main__":
    
    test_dir_dogs = "/media/data/dogs_vs_cats_small/test/dogs"
    fnames = [os.path.join(test_dir_dogs, fname) for fname in os.listdir(test_dir_dogs)]

    # convert from PIL to np.array (cast to np.int in the range [0, 255])
    img_path = fnames[4]
    img = image.load_img(img_path)
    x = image.img_to_array(img).astype(int)
    plt.figure()
    plt.imshow(x)
    
    # convert from PIL to np.array (as np.float32 in the range [0, 1])
    img_path = fnames[5]
    img = image.load_img(img_path)
    x = image.img_to_array(img) * 1./255
    plt.figure()
    plt.imshow(x)
    
    plt.show()
```


Here is the result:

![](img/fig_dog_1.png)

![](img/fig_dog_2.png)



## Using `ImageDataGenerator`

Consider this snippet:

```python
from keras.preprocessing.image import ImageDataGenerator

test_dir = "/media/data/dogs_vs_cats_small/test/dogs"
test_datagen = ImageDataGenerator(rescale=1./255)
test_generator = test_datagen.flow_from_directory(test_dir, 
                                                  target_size=(150, 150),
                                                  batch_size=32,
                                                  class_mode="binary"
                                                  )
# print the first batch
for batch in test_generator:
    print(batch)
	break
```

If you run it, you will get this error:

```
Found 0 images belonging to 0 classes.
```

[It turns out](https://github.com/keras-team/keras/issues/3946) that this error is thrown out when the path of the images (`test_dir` in this case), does not have the structure expected by Keras. In this example, `test_dir` contains images of dogs only. However, the content of `test_dir` should be divided into directories, one for each class:

```
dogs_vs_cats_small/
	test/
		dogs/		# class 1
		cats/		# class 2
	train/
		dogs/		# class 1
		cats/		# class 2
	validation/
		dogs/		# class 1
		cats/		# class 2						
```

Therefore, just change `test_dir` as follows to fix it:

```python
test_dir = "/media/data/dogs_vs_cats_small/test/dogs"
```

Now, you will get this message:

```
Found 1000 images belonging to 2 classes.
```

This is the full code for completeness:

```python
from __future__ import print_function
from keras.preprocessing.image import ImageDataGenerator

if __name__ == "__main__":
    
    test_dir = "/media/data/dogs_vs_cats_small/test"
    test_datagen = ImageDataGenerator(rescale=1./255)
    test_generator = test_datagen.flow_from_directory(test_dir, 
                                                    target_size=(150, 150),
                                                    batch_size=32,
                                                    class_mode="binary"
                                                    )
    # print the first batch
    for batch in test_generator:
        print(batch)
        break
```



## Using `fit` and `fit_generator`

You can specify the training samples (`traing_features`) and their labels (`train_labels`) in `fit` as follows:

```python
# Listing 5.18 Defining and training the densely connected classifier
history = model.fit(train_features,
                    train_labels,
                    epochs=30,
                    batch_size=20,
                    validation_data=(validation_features, validation_labels))
```

You can also employ a generator (`ImageDataGenerator`) to provide the training samples and labels (`train_generator`):

```python
# Listing 5.21 Training the model end to end with a frozen convolutional base
history = model.fit_generator(train_generator,
                              steps_per_epoch=100,
                              epochs=30,
                              validation_generator=validation_generator,
                              validation_steps=50)
```

From the above snippets, it seems that the `fit` function depends on how the images are given:

- If the images and labels are given as two individual arrays (e.g., `train_features` and `train_labels`), use `fit`.
- If the images and labels are given as a generator (e.g., `train_generator`), use `fit_generator`.

Pretty obvious, right? Well, after checking my code, I found this:

```python
# Listing 5.8 Fitting the model using a batch generator
history = model.fit(                               # my mistake: fit instead of fit_generator
            train_generator,
            steps_per_epoch=100,
            epochs=30,
            validation_data=validation_generator,
            validation_steps=50
            )
```

I used `fit` instead of `fit_generator`. However, the code worked as expected. According to the [documentation](https://keras.io/models/sequential/) (shorten for clarity):

> ```
> fit(x=None, y=None, steps_per_epoch=None, validation_data=None, validation_steps=None, ...)
> ```
> `x` can be:
>
> - A Numpy array (or array-like)
> - A generator or `keras.utils.Sequence` returning `(inputs, targets)` or `(inputs, targets, sample _weights)`
>
> If `x` is a generator, or `keras.utils.Sequence` instance, `y` should not be specified (since targets will be obtained from `x`). 



> ```
> fit_generator(generator, steps_per_epoch=None, validation_data=None, validation_steps=None, ...)
> ```
>
> - `generator` A generator or an instance of `keras.util.Sequence`
> - `steps_per_epoch` Total number of steps before stopping the `yield` loop of `generator`. It should typically be equal to `ceil(num_samples/batch_size)`.
> - `epochs` Number of epochs to train the model.
> - `validation_data` this can be either
>   - a generator or a `Sequence` object
>   - tuple `(x_val, y_val)`
>   - tuple `(x_val, y_val, val_sample_weights)`
> - `validation_steps` only relevant if `validation_data` is a generator. Total number of steps (batches of samples) to yield from `validation_data` before stopping at the end of every epoch. It should typically be equal to `ceil(num_val_samples/batch_size)`.

That is, it seems that both functions, `fit` and `fit_generator` accept a generator as input. However, `fit_generator` does not accept `x` and `y` as input like `fit` does. Anyway, I am not completely sure if you should use them interchangeably because the meaning of `steps_per_epoch` and `validation_steps` differs in `fit` and `fit_generator`:



> ```
> fit(steps_per_epoch=None, validation_steps=None, ...)
> ```
>
> - `steps_per_epoch`: Total number of steps (batches of samples) before declaring one epoch finished and starting the next epoch.  (**i.e., it did not mention that this parameter is only relevant if `x` is a generator**)
> - `validation_steps`: Only relevant if `steps_per_epoch` is specified. Total number of steps (batches of samples)
>   to validate before stopping.
> - `validation_steps`: Only relevant if `validation_data` is provided and is a generator. Total number of steps (batches of samples) to draw before stopping when performing validation at the end of every epoch. (**this is similar to `validation_steps` in `fit_generator`**)

Although these parameters, `steps_per_epoch` and `validation_steps`, can be employed in `fit` and `fit_generator`, their meanings seem to differ. For this reason, I think that :

1. You should use `fit_generator` when the training samples and labels are given as a generator. If so, do not forget to specify `steps_per_epoch=ceil(num_samples/batch_size)`. The same rule applies for `validation_data`. If it is a generator, use `validation_steps=ceil(num_val_samples/batch_size)`.
2. You should use `fit` is the training samples and labels are stored as two arrays, `x` and `y`. If so, you should consider to specify a `batch_size` (otherwise, the default value will be used, `batch_size=32`).




## Constructing your network according to the type of problem

In [1] page 114, the author gives the following table to help you chose the right last-layer activation and loss function for your model.

| Problem type                            | Last-layer activation | Loss function                  | Example                                                      |
| --------------------------------------- | --------------------- | ------------------------------ | ------------------------------------------------------------ |
| Binary classification                   | `sigmoid`             | `binary_crossentropy`          | Binary classification of movie reviews from the IMDB dataset, page 68. |
| Multiclass, single-label classification | `softmax`             | `categorical_crossentropy`     | Classification of news into 46 mutually exclusive topics, page 78. |
| Multiclass, multi-label classification  | `sigmoid`             | `binary_crossentropy`          | None                                                         |
| Regression to arbitrary values          | None                  | `mse`                          | Prediction of house prices, page 85.                         |
| Regression to values between 0 and 1    | `sigmoid`             | `mse` or `binary_crossentropy` | None                                                         |

From [2] page 276:

> **Activation functions**
>
> In most cases you can use the **ReLU** activation function in the hidden layers (or one of its variants). It is a bit faster to compute than other activation functions, and Gradient Descent does not get stuck as much on plateaus, thanks to the fact that it does not saturate for large input values (as opposed to the logistic function or the hyperbolic tangent function, which saturates at 1).
>
> For the output layer, the **softmax** activation function is generally a good choice for classification tasks when the classes are mutually exclusive. When they are not mutually exclusive (or when there are just two classes), you generally want to use the **logistic** function function. For regression tasks, you can simply use **no activation function** at all for the output layer.



Additional notes:

| Loss function         | Metric | Notes                                                        |
| --------------------- | ------ | ------------------------------------------------------------ |
| `binary_crossentropy` | `acc`  | Be sure to use `class_mode='binary'` when using a generator in binary classification. From [1], page 151: *Because you use `binary_crossentropy` loss, you need binary labels*. |
|                       |        |                                                              |
|                       |        |                                                              |
|                       |        |                                                              |
|                       |        |                                                              |
|                       |        |                                                              |
|                       |        |                                                              |
|                       |        |                                                              |
|                       |        |                                                              |











## Binary classification

[colocar ejemplo completo] para mostrar si se usa o no one-hot encoding





## Reshaping an array

You may need to reshape a Numpy array before feeding it into a model. In [1], Chollet uses at least two approaches to this:

```python
# pages 174, 161
# Adds a dimension to transform the array into a batch of size (1, 224, 224, 3)
x = np.expand_dims(x, axis=0)  
```

```python
# page 140
# Reshapes it to (1, 150, 150, 3)
x = x.reshape((1,) + x.shape)
```

In both cases, the author adds a new dimension to transform the input data into a batch of shape `(batch_size, height, width, channels)`.

In [2], Géron uses this approach:

```python
# page 238
y = tf.constant(housing.target.reshape(-1, 1), dtype=tf.float32, name="y")  # shape (20640, 1)
```

The important part is `housing.target.reshape(-1, 1)`.  Here, `housing.target.shape` is `(20640,)`, a one-dimensional array. This array contains target values for housing pricing prediction (a regression problem). Although, the target vector is expected to be a column vector, that is, a Numpy array of shape `(samples, 1)` . To do so, the author reshapes the labels using `reshape(-1, 1)`. As he explains:

> Note that `housing.target` is a 1D array, but we need to reshape it to a column vector to compute `theta`. Recall that Numpy's `reshape()` functions accepts `-1` (meaning *"unspecified"*) for one of the dimensions: that dimension will be computed based on the array's length and the remaining dimensions.

In [3], Rosebrock uses this approach (p. 144):

```python
def fit_partial(self, x, y):
	A = [np.atleast_2d(x)]
```

`np.atleast_2d` adds a new dimension to a one-dimensional array. For instance: `x = np.array([1,2,3])` is converted to `np.array([[1,2,3]])`. That is, its shape changes from `(3, )` to `(1, 3)`.



Finally, [4] [Kolter and Madry](https://adversarial-ml-tutorial.org/introduction/) uses this approach:

```python
from PIL import Image
from torchvision import transforms

# read the image, resize to 224 and convert to PyTorch Tensor
pig_img = Image.open("pig.jpg")
preprocess = transforms.Compose([
   transforms.Resize(224),
   transforms.ToTensor(),
])
pig_tensor = preprocess(pig_img)[None,:,:,:]
```

Here, they add a dimension, so the shape of the tensor changes from `torch.Size([3, 224, 224])` to `torch.Size([1, 3, 224, 224])`.



**Examples**

Below we show different approaches for reshaping an array. The output of all examples is the same. 

```python
# see Chollet, p 174
new_targets_row = np.expand_dims(targets, axis=0)
new_targets_col = np.expand_dims(targets, axis=1)

print(new_targets_row)
print(new_targets_row.shape)

print(new_targets_col)
print(new_targets_col.shape)
```

This is the output for `new_targets_row`:

```
[[4.526 3.585 3.521 3.413 3.422]]
(1, 5)
```

and the output for `new_targets_col`:

```
[[4.526]
 [3.585]
 [3.521]
 [3.413]
 [3.422]]
(5, 1)
```

Now, check the remaining examples:

```python
# see Chollet, p 174
new_targets_row = targets.reshape((1,) + targets.shape)
new_targets_col = targets.reshape(targets.shape + (1,))
```

```python
# see Geron, p 238
new_targets_row = targets.reshape(1, -1)
new_targets_col = targets.reshape(-1, 1)
```

```python
# see https://adversarial-ml-tutorial.org/introduction/
new_targets_row = targets[None, :]
new_targets_col = targets[:, None]
```



## Splitting a dataset

In [1] p. 98, Chollet explains how to split your data into training and validation sets, and reviews three recipes to this end: simple hold-out validation, K-fold cross-validation, and iterated K-fold cross-validation with shuffling. 

The training set contains the instances for training the model. The validation set is employed for evaluating the performance of the trained model and parameter tunning. As pointed out by the author, you should not tune your model based on the test set.

**Simple hold-out validation**

Set apart some fraction of your data as your test set. Then, separate again the remaining instances into two sets: one for training and one for validation.

[image]



**K-fold validation**

Set apart some fraction of your data as your test set. Then, split your remaining data into `k` partitions of equal size. For each partition `i`, train a model on the remaining `k-1` partitions, and evaluate it on partition `i`. The performance of the model is the average of the `k` scores obtained. According to the author, this method is helpful when the performance of the model shows significant variance based on your train-test split.



**Stratified sampling**

In [2] p. 51, Géron remarks that the training set must represent the overall population (i.e., the whole dataset) to avoid sampling bias:

> The US population is composed of 51.3% female and 48.7% male, so a well-conducted survey in the US would try to maintain this ratio in the sample: 513 female and 487 male. This is called **stratified sampling**: the population is divided into homogeneous subgroups called *strata*, and the right number of instances is sampled from each stratum to guarantee that the test set is representative of the overall population.



```python
# third approach: stratified sampling
from sklearn.model_selection import StratifiedShuffleSplit

split = StratifiedShuffleSplit(n_splits=1,
                               test_size=0.2,
                               random_state=42)

# add variable to create five categories based on "median_income"
housing["income_cat"] = pd.cut(housing["median_income"],
                              bins=[0, 1.5, 3, 4.5, 6, np.inf],
                              labels=[1,2,3,4,5])

for train_index, test_index in split.split(housing, housing["income_cat"]):
    strat_train_set = housing.loc[train_index]
    strat_test_set = housing.loc[test_index]
```



**Bottom line** I think that we must use K-fold cross-validation (or iterated K-fold cross-validation if there are enough computational resources). Although, we also must preserve the proportion among groups in the dataset. In [2], the author address a regression problem, so he decide to create five categories regarding a attribute called `median_income` for predicting a single real value, `median_house_value`. In the documentation of [StratifiedShuffleSplit](https://scikit-learn.org/stable/modules/generated/sklearn.model_selection.StratifiedShuffleSplit.html), the authors use the labels to maintain proportions among classes. Although this last strategy makes sense, I am not sure if it will work when the dataset (or the population) is imbalanced.



## How to evaluate and tune your model using cross-validation

In [1] p. 89, Chollet evaluates a model using K-fold cross-validation with `k=4` folds (see Listing 3.27 K-fold validation, Listing 3.28 Saving the validation logs at each fold, Listing 3.29 Building the history of successive mean K-fold validation scores, and Listing 3.30 Plotting validation scores). The author defines these sets: `val_data` and `val_targets` (validation set), and `partial_train_data` and `partial_train_targets` (training set). We use cross-validation, so the partition `i` is the validation set, whereas the remaining `k-1` partitions comprises the training set.

```python
k = 4
for i in range(k):
    # split data into validation and training sets
    
    # build keras model
    model = build_model()
    
    # training
    history = model.fit(partial_train_data, partial_train_targets,   # training set
                        epochs=num_epochs, batch_size=1, verso
                   
                       )
```

----

**Question** How to use cross-validation along with `DataImageGenerator`s? We can use `DataImageGenerator` for feeding data into a model with `fit_generator`:

```python
# Listing 5.21 Training the model end to end with a frozen convolutional base
history = model.fit_generator(train_generator,
                              steps_per_epoch=100,
                              epochs=30,
                              validation_generator=validation_generator,
                              validation_steps=50)
```

However, how can we use K-fold cross-validation for yielding data into the model via a generator?

---

**Question** How to keep the proportion among classes when using cross-validation (ie, how to do stratified cross-validation with generators). After searching, it turns out that we can use cross-validation and keep the proportions among classes. That sampling method is called **stratified K-fold cross-validation** or `StratifiedKFold` in [Scikit-learn](https://scikit-learn.org/stable/modules/generated/sklearn.model_selection.StratifiedKFold.html). From this [post](https://github.com/keras-team/keras/issues/1711#issuecomment-185801662), it seems relatively simple to use `StratifiedKFold` for feeding data into a model with `model.fit`:

```python
from sklearn.cross_validation import StratifiedKFold

def load_data():
    # load your data using this function

def create model():
    # create your model using this function

def train_and_evaluate_model(model, train_data, train_labels, val_data, val_labels):
    # fit and evaluate here
    history = model.fit(train_data, train_labels,					# training set
                        validation_data=(val_data, val_labels),		# validation set
                        epochs=..., batch_size=...)					# other parameters
   	
    # return accuracy throughout the training process 
    return history.history["acc"]

if __name__ == "__main__":
    n_folds = 10
    data, labels, header_info = load_data()
    skf = StratifiedKFold(labels, n_folds=n_folds, shuffle=True)
    hist = [None]*n_folds
    
    # for each fold    
    for i, (train, val) in enumerate(skf):
    	
        # create a fresh model
        model = None
        model = create_model()

        # train using the i-th fold
        h = train_and_evaluate_model(model, data[train], labels[train], data[val], labels[val])
            
        # save metrics
        hist[i] = list(h)
```

These [other](https://www.kaggle.com/stefanie04736/simple-keras-model-with-k-fold-cross-validation) [posts](https://medium.com/the-owl/k-fold-cross-validation-in-keras-3ec4a3a00538) describe similar approaches. Note that each time `train_and_evaluate_model` is called, we pass a different fold:

```python
train_and_evaluate_model(model, data[train_fold_0], data[val_fold_0], ...)
train_and_evaluate_model(model, data[train_fold_1], data[val_fold_1], ...) 
...
train_and_evaluate_model(model, data[train_fold_9], data[val_fold_9], ...) 
```

Although this works (I think so), the whole dataset is loaded and stored in `data`. If the dataset is large, then it could be difficult or inconvenient to store it in a single array. Instead, we should use a generator and load batches of the dataset. Given the previous example, we can do that as follows:

```python
from sklearn.cross_validation import StratifiedKFold

def get_generator(directory, indices, batch_size):
    # return batch of instances

if __name__ == "__main__":
    train_dir = "images/train"		# it contains other dirs, like images/train/class1...classn
    n_folds = 10
    data, labels, header_info = load_data(train_dir)
    skf = StratifiedKFold(labels, n_folds=n_folds, shuffle=True)
    hist = [None]*n_folds
    
    # for each fold    
    for i, (train, val) in enumerate(skf):
    	
        # create a fresh model
        model = None
        model = create_model()
        
        # create generators
        train_gen = get_generator(train_dir, train)
        val_gen = get_generator(train_dir, val)

        # train using the i-th fold
        h = train_and_evaluate_model(model, train_gen, val_gen)
            
        # save metrics
        hist[i] = list(h)
```
Here, we define a custom generator. We recommend to read [1] p. 211, Listing 6.33 Generator yielding timeseries samples and their targets.

```python
def get_generator(directory, indices, batch_size=20, shuffle=False):
    """
    directory: str, directory to read images/instances from
    indices:   array, contains the indices of the instances that can be sampled and yielded
    
    Create a generator as follows
    gen = get_generator(dir, indices)
    
    We can iterate as follows:
    for batch, labels in next(gen):
    	print(batch), print(labels)
    	break
    
    Every time we call next(gen), we sample batch_size values from inidices randomly.
    We then load the instances of the sampled values and yield them. This way, we do not
    need to load the whole dataset.
    
    indices can be sampled using StratifiedKFold, KFold, or any other sampling technique.
    Here, we only pick batch_size random values from indices each time.
    """
    
    while True:
        
        if shuffle:
            
        
        batch = indices[start:end]
        
        samples, targets = load_batch(batch)
        
        yield samples, targets

def load_batch(directory, batch, labels):
    n = len(batch)
    samples = [None]*n
    labels = [None]*n
    
    for i, id in enumerate(batch):
        samples[i] = load_image(directory, "image_%d.jpg" % id)
        labels[i] = ...
        # ojo! aqui falta saber como identificar el nombre de la clase a partir de los indices
        # creo que lo podemos obtener a partir de los indices y un arreglo labels
        # label_name = LABELS[labels[id]]
        # recuerda
        # skf = StratifiedKFold(labels, n_folds=n_folds, shuffle=True)
        # genera un arreglo (train, val), donde train y val son arreglos de indices 
        # (de una matriz data),
        # por lo tanto, si accedo a
        # 
        # for i, (train, val) in enumerate(skf):
        #   val[0]         es el indice de primera instancia del validation set
        #   data[val[0]]   es la primera instancia del validation set
        #   labels[val[0]] es el label de la primera instancia
        # podemos usar esa info para obtener el nombre de la clase (y de su directorio)
        # en load_batch(dir, batch_indices, batch_labels)
        # LABELS = ("CAT", "DOG")
        # 
        #   label_name = LABELS[]
    	# creo que es muy complicado, mejor debemos usar un csv con los filenames y los labels de
        # cada instancia, y de ahi usar StratifiedKFold para generar los indices de cada split
        # (cargar un csv grande es mas sencillo que un dataset grande), y pasarle los splits
        # a load_batch para que lea las instancias
        # continua con esto manana
```

A few things to notice:

- We use `train_dir` for creating both `train_gen` and `val_gen`. Remember we first set aside a fraction of the dataset as a test set. The test set is stored in `"images/test"`. We then split the remaining data using `StratifiedKFold`. Since the remainig data is employed for training the model, there is no need to use separate directories, like `"images/train"` and `"images/val"`: All the data is training data and the splits (folds) are done with `StratifiedKFold`.

## References

[1] Chollet. Deep learning with Python.

[2] Géron. Hands-On Machine Learning with Scikit-Learn & Tensorflow.

[3] Rosebrock. Deep Learning for Computer Vision.

[4] [Adversarial ML](https://adversarial-ml-tutorial.org/introduction/) 