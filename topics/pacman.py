# pacman.py
import gym
import time
import matplotlib.pyplot as plt
import matplotlib.animation as animation

# load the ms pacman environment
env = gym.make("MsPacman-v0")

# initialize the environment by calling its reset() method
# this returns an observation
obs = env.reset()

import ipdb; ipdb.set_trace()

# visualize environment
env.render()

# lets move
"""
env.reset()
env.render()
input(">")

for step in range(110):
    env.step(3) # left
    env.render()
    time.sleep(0.5)

for step in range(40):
    env.step(8) # lower-left
    env.render()
    time.sleep(0.5)
"""

# the step() function returns these objects
obs, reward, done, info = env.step(0)
print("objs.shape: {}".format(obs.shape))
print("reward: {}".format(reward))
print("done: {}".format(done))
print("info: {}".format(info))

# let's play one full game (with 3 lives)
# every 10 steps, we choose a random direction
frames = []
n_max_steps = 1000
n_change_steps = 10

obs = env.reset()

import ipdb; ipdb.set_trace()

for step in range(n_max_steps):
    # save frame
    img = env.render(mode="rgb_array")
    frames.append(img)

    # change direction
    if step % n_change_steps == 0:
        action = env.action_space.sample()  # play randomly
    
    # move
    obs, reward, done, info = env.step(action)

    if done:
        break

def update_scene(num, frames, patch):
    patch.set_data(frames[num])
    return patch, 

def plot_animation(frames, repeat=False, interval=40):
    plt.close()
    fig = plt.figure()
    patch = plt.imshow(frames[0])
    plt.axis("off")
    return animation.FuncAnimation(fig, update_scene, fargs=(frames, patch),
                                    frames=len(frames), repeat=repeat, interval=interval)

video = plot_animation(frames)
plt.show()

# free up resources
env.close()