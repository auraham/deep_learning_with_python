# Topics

This directory organizes references by topic.









## Feature engineering

- [How to Win a Data Science Competition: Learn from Top Kagglers](https://www.coursera.org/learn/competitive-data-science/home/welcome). Check section: Feature preprocessing and feature generation with respect to models.



## Decision boundaries

- https://scikit-learn.org/stable/auto_examples/classification/plot_classifier_comparison.html





## Neural networks

### Initialization of weights

- [Efficient Backprop](http://yann.lecun.com/exdb/publis/pdf/lecun-98b.pdf), see Section 4.6



### Recommendations for training a neural network

- [Efficient Backprop](http://yann.lecun.com/exdb/publis/pdf/lecun-98b.pdf), see conclusions:

> According to the recommendations mentioned above, a practitioner facing a multi-layer neural net training problem would go through the following steps:
>
> - shuffle the examples
> - center the input variables by subtracting the mean
> - normalize the input variable to a standard deviation of 1
> - if possible, decorrelate the input variables
> - pick a network with the sigmoid function shown in figure 4
> - set the target values within the range of the sigmoid, typically +1 and -1
> - initialize the weights to random values as prescribed by Equation 16.

That should be taken with a grain of salt, since that methodology was proposed in 1998. For instance, there are other ways to initialize the weights (see comments above in this file).









## Dangers of machine learning





## Plotting

- Plotting histogram of categorical features:

```
train["Embarked"].value_counts().plot(kind="bar", title="Embarked");
```



You can see other methods [here](https://stackoverflow.com/questions/31029560/plotting-categorical-data-with-pandas-and-matplotlib).







## Pandas

- Equivalence between `value_counts` and `groupby().size()`:

```python
# these two lines return the same result
train.groupby("Embarked").size()
train["Embarked"].value_counts()
```

