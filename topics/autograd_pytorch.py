# autograd_pytorch.py
import torch

def model(t_u, w, b):
    return w * t_u + b

def loss_fn(t_p, t_c):
    squared_diffs = (t_p - t_c) ** 2
    return squared_diffs.mean()


t_c = [0.5,  14.0, 15.0, 28.0, 11.0,  8.0, 3.0,  -4.0,  6.0, 13.0, 21.0]  # celsius, page 71
t_u = [35.7, 55.9, 58.2, 81.9, 56.3, 48.9, 33.9, 21.8, 48.4, 60.4, 68.4]  # unknown units
t_c = torch.tensor(t_c)
t_u = torch.tensor(t_u)

params = torch.tensor([1.0, 0.0], requires_grad=True)
loss = loss_fn(model(t_u, *params), t_c)

loss.backward()

print(params.grad)