# autograd_pytorch_example2.py
import torch

def mse_loss(y_true, y_pred):
    return ((y_true - y_pred) ** 2).mean()

def sigmoid(x):
    return 1.0 / (1.0 + torch.exp(-x))

def forward(x, w1, w2, w3, w4, w5, w6, b1, b2, b3):
    # eg, x = [0, 0]
    h1 = sigmoid(w1*x[0] + w2*x[1] + b1)
    h2 = sigmoid(w3*x[0] + w4*x[1] + b2)
    o1 = sigmoid(w5*h1 + w6*h2 + b3)
    return o1


y_true = torch.tensor([1.0, 0.0, 0.0, 1.0])
y_pred = torch.tensor([0.0, 0.0, 0.0, 0.0])

mse_loss(y_true, y_pred) # tensor(0.5)

# forward pass
w1 = torch.tensor(1.0, requires_grad=True)
w2 = torch.tensor(1.0, requires_grad=True)
w3 = torch.tensor(1.0, requires_grad=True)
w4 = torch.tensor(1.0, requires_grad=True)
w5 = torch.tensor(1.0, requires_grad=True)
w6 = torch.tensor(1.0, requires_grad=True)
b1 = torch.tensor(0.0, requires_grad=True)
b2 = torch.tensor(0.0, requires_grad=True)
b3 = torch.tensor(0.0, requires_grad=True)
x  = torch.tensor([-2.0, -1.0])

y_true = torch.tensor(1)
y_pred = forward(x, w1, w2, w3, w4, w5, w6, b1, b2, b3) # tensor(0.5237)
loss = mse_loss(y_true, y_pred)
loss.backward()

print(w1.grad)
print(w2.grad)
print(w3.grad)

# este ejemplo devuelve el mismo resultado que
# https://victorzhou.com/blog/intro-to-neural-networks/
# pero aun me quedan muchas dudas:
# como sabe pytorch que los gradientes (derivadas parciales del Loss) son del Loss con respecto a los weights
# o mejor dicho, como sabe que x no es ajustable? (no se como preguntar mi duda)
# hay forma de ver el computational graph y los gradientes en cada nodo?
# quiza esto ayude:
# https://stackoverflow.com/questions/65151627/computing-intermediate-gradients-using-backward-method-in-pytorch
# https://blog.paperspace.com/pytorch-hooks-gradient-clipping-debugging/
# como se compara esto con keras? creo que no se puede hacer esto alla