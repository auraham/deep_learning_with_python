# Definitions and questions

In the following, we give questions about machine learning.



## Evaluation of the model

### How can you tell that your model is overfitting or underfitting the data?

There are many ways:

- Use cross-validation [Geron] p. 70
- Use learning curves [Geron] p.126, [Jason] p. 24

Check the notebooks `learning_curves.ipynb`, `cross_validation.ipynb` in `topics`.





# References

- [Geron] Hands-On Machine Learning with Scikit-Learn & TensorFlow. Aurélien Géron.
- [Jason] Better Deep Learning. Jason Brownlee.