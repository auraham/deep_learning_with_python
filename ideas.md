# Ideas



## Cost function

En el video de Logistic Regression Cost Function, Andrew Ng dice esto en el quiz al final del video:



> **Quiz** What is the difference between the cost function and the loss function for logistic regression?
>
> **Answer** The loss function computes the error for a single training example; the cost function is the average of the loss function of the entire training set.

Distingue entre dos funciones, la funcion loss $L$ y la funcion de costo $J$. En resumen, la relacion entre funciones es la siguiente:


$$
J(w, b) = \frac{1}{m}\sum_{i=1}^{m}L(\hat{y}^{(i)}, y^{(i)}) = -\frac{1}{m}\sum_{i=1}^{m}[y^{(i)} \log(\hat{y}^{(i)}) + (1 - y^{(i)}) \log(1 - \hat{y}^{(i)})]
$$




Estaba pensando, habra casos en donde la sumatoria $\frac{1}{m}\sum$ contenga elementos (valores del loss) que sean o muy grandes o muy pequeños, por ejemplo:
$$
J(w, b) = \frac{1}{m}\sum_{i=1}^{m}L(\hat{y}^{(i)}, y^{(i)}) = \frac{1}{m}[0.1, 0.2, 0.1, ..., 100, , ..., 0.1, 0.2]
$$
En ese caso, hay un patron que tiene un error de 100. Esto podria afectar la convergencia del SGD  o el ajuste de los pesos:

- Qué pasa si removemos/ignoramos el patron que genero ese error outlier? 



De acuerdo con [Lecun]

> 4.2 Shuffling the examples
>
> Networks learn the fastest from the most unexpected sample. Therefore, it is advisable to choose a sample at each iteration that is the most unfamiliar to the system [...] a very simple trick that crudely implements this idea is to simply choose successive examples that are from *different* classes since training examples belonging to the same class will most likely contain similar information.
>
> Another heuristic for judging how much new information a training example contains is to examine the error between the network output and the target value when this input is presented. A large error indicates that this input has not been learned by the network and so contains a lot of new information. Therefore, it makes sense to present this input more frequently. [...] A method that modifies the probability of appearance of each pattern is called an *emphasizing scheme*. 
>
> However, one must be careful when perturbing the normal frequencies of input examples because this changes the relative importance that the network places on different examples. This may or may be not desirable. For example, *this technique applied to data containing outliers can be disastrous* because outliers can produce large errors yet should not be presented frequently. 



Podemos rescatar lo siguiente del parrafo anterior:

- Puede resultar conveniente presentar ejemplos (training examples) que produzcan un error grande, relativo a los errores de los demas patrones. Es decir, seria bueno tener una lista de errores como : $\frac{1}{m}[0.1, 0.2, 0.1, ..., 100, , ..., 0.1, 0.2]$.
- Antes de usar un *emphasizing scheme* [es decir, presentar patrones con un gran error (ie, un gran contenido de informacion nueva para la red)], es conveniente remover los patrones outliers del dataset, para evitar una perturbacion grande (ie un error grande).





## Sentiment analysis pero para noticias

Estaba pensando en que puedo aplicar la misma idea de clasificacion de texto/reviews como positiva y negativa pero en el contexto de noticias y links.

Puedo extraer todas las noticias de hackernews y clasificarlas en positivas (me gustan) o negativas (no me interesan). 

Como dataset, puedo usar todos mis links en pocket (aunque solo contendran ejemplos positivos)

Como ejemplos negativos, quedaria pendiente hacer el dataset.

El procedimiento de clasificacion de noticias debe ser similar al procedimiento de clasificacion de reviews/analysis de sentimientos:

	- Preprocesar el texto: steaming, tokenizing, ...
	- Usar bag of words / RNN / CNN / Word2Vec / GloVE
 - Obtener un valor binario
   	- 0: noticia no interesante
   	- 1: noticia interesante

Lo que aun no se como hacer es si haya problema con la longitud del texto. Dado que los links/titulos de las noticias son de longitud variable, no se si la red/el modelo tenga problemas con una entrada de longitud variable. Recuerdo que Chollet se quedaba solo con los primeros n caracteres de una review, por lo que la entrada/longitud era constante.