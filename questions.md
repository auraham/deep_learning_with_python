# Questions



## Por qué debemos shuffling los training samples antes de presentarlos a la red?

Mi intuicion es la siguiente. Supon que no barajeas los samples y los presentas en orden a la red. Por ejemplo, presentas todos los samples de la clase A y actualizas los pesos. Luego, presentas todos los patrones de la clase B y actualizas los pesos. Repites asi por cada clase. Creo que en este esquema SGD puede quedar atrapado en un optimo local, en el cual la red se especialice en clasificar samples de la clase A y sea muy mala para clasificar samples de otras clases.

Para evitar caer en un optimo local (o al menos no hacerlo tan rapido como en el caso anterior), se recomienda shufflear los samples. 

Seria bueno revisar la literatura sobre este punto.



## Por qué un GPU acelera el entrenamiento de la red?

No estoy seguro de esto, pero es porque podemos usar más de un training example al modelo en el forward y backward pass. En el curso de Ng:



> It turns out that when you implement a neural network there are some techniques that are going to be really important. For example, if you have a training set of $m$ training examples, you might be used to processing
> the training set by having a for loop step through your $m$ training examples. But it turns out that when you're
> implementing a neural network, you usually want to process your entire training set without using an explicit for loop to loop over your entire training set.

Esto me da a entender que podemos pasarle un batch (el dataset completo) o un mini-batch (un subset del dataset) al modelo. Para procesar dicho subset, usamos un GPU.





## What is the difference between one-hot encoding and tokenization?

**tl;dr** ...

In [1], the author explains at least two approaches for classification of text:

- using one-hot encoding (i.e., converting a string into a binary vector)

```python
# listing 3.3
from keras import models
from keras import layers

model = models.Sequential()
model.add(layers.Dense(16, activation="relu", input_shape=(10000,)))
model.add(layers.Dense(16, activation="relu"))
model.add(layers.Dense(1, activation="sigmoid"))
```

- using embeddings (i.e., converting a string into a float vectors)  in Section 6.1.3.

```python
# listing 6.12
from keras.models import Sequential
from keras.layers import Embedding, Flatten, Dense

model = Sequential()
model.add(Embedding(max_words, embedding_dim, input_length=maxlen))
model.add(Flatten())
model.add(Dense(32, activation="relu"))
model.add(Dense(1, activation="sigmoid"))

# listing 6.13
model.layers[0].set_weigths([embedding_matrix])
model.layers[0].trainable = False                 # freeze layer
```

For the later approach, the author introduces a procedure called *tokenization*. The tokenization is employed in Section 6.1.3. However, after reading both approaches (binary classification via one-hot encoding or via tokenization), I got confused. In the following, we show the output of both approaches to get a better understanding of how they work.

----

Let's start with one-hot encoding. This procedure is employed to preprocess the input examples before fed them into the network:

```
example here
```

Consider the following example. The following block shows a few words (strings) after performing the one-hot encoding procedure:

```
example output here
```

Now let's talk about tokenization. Like one-hot encoding, tokenization is employed to preprocess the input examples before fed them into the network:

```python
# listing 6.9
from keras.preprocessing.text import Tokenizer
from keras.preprocessing.sequence import pad_sequences
import numpy as np

maxlen = 100
training_samples = 200
validation_samples = 10000
max_words = 10000

tokenizer = Tokenizer(num_words=max_words)
tokenizer.fit_onn_texts(texts)
sequences = tokenizer.texts_to_sequences(texts)

word_index = tokenizer.word_index
print("Found %s unique tokens" % len(word_index))
```

Now, **here is the important part**. The following block shows the same words employed in the previous example./The following block shows the output of the tokenization procedure on a few input examples:

```
put example here
```

----

**Note** Add the output of the previous procedure to highlight the difference between one-hot encoding and tokenization.

----

After tokenization, the words (strings) are then converted into float vectors using the precomputed embeddings from GloVe. You can think of if as a dictionary (`embeddings_index`) that maps words (strings) to float vectors of shape `(embedding_dim, )`:

```python
# listing 6.10
glove_dir = "/home/user/Downloads/glove.6B"

embeddings_index = 


```

This is the partial content of  `embeddings_index`:

```
put example
empeddings_index = 
{
"word": [vector], 
"word": [vector], 
}
```

Until now, this is what we have done:

1. Tokenize the input text examples
2. Load precomputed embeddings as a dictionary (`empeddings_index`)

Now, we will use empeddings_index to convert the tokenized input examples (strings) into float vectors, as shown in the next listing:

----

**Note** I am not sure about this, but notice that if the input examples contains words that are not available in `embeddings_index`, we can ignore them. In Listing 6.11, the author says that:

> Words not found in the embedding index will be all zeros.

However, I hypothesize that a better approach could be to skip such missing words. Otherwise, we could end up with multiple zero-vectors, `[0, 0,..., 0]`, that incorrectly represents the same missing words.

----

```
# listing 6.11
```

So far, we have shown how to use one-hot encoding and tokenization for binary classification from text samples. To avoid confusion between these concepts, we summarize the key aspects of both approaches:

- One-hot encoding is a way to transform text samples (strings) into binary vectors.
- Tokenization is a way to ...



## Testing with values beyond a the range given when training an algorithm

In [2]:

> Remove those districts (i.e., examples) from the training test (and also from the test set, since your system should not be evaluated poorly if it predicts values beyond $500,000).

Is there a way or good practice to evaluate our model using instances with attribute values not in the expected range?



## Origin of commonly-used mean and standard deviation values for normalization

[This post](https://pytorch.org/docs/stable/torchvision/models.html) explains how to normalize an image using PyTorch:

```python
normalize = transforms.Normalize(mean=[0.485, 0.456, 0.406],
                                 std=[0.229, 0.224, 0.225])
```

You may wonder where those values came from. That post also explained that such values were obtained by using a procedure similar to this one:

```python
import torch
from torchvision import datasets, transforms as T

transform = T.Compose([T.Resize(256), T.CenterCrop(224), T.ToTensor()])
dataset = datasets.ImageNet(".", split="train", transform=transform)

means = []
stds = []
for img in subset(dataset):
    means.append(torch.mean(img))
    stds.append(torch.std(img))

mean = torch.mean(torch.tensor(means))
std = torch.mean(torch.tensor(stds))
```

However, the concrete `subset` is lost (you can read [these](https://github.com/pytorch/vision/pull/1965) [discussions](https://github.com/pytorch/vision/issues/1439)).

I am pretty sure that this is not the first time I see those values. That normalization also appears in [this post](https://adversarial-ml-tutorial.org/introduction), where the authors give a brief introduction to adversarial examples:

```python
# values are standard normalization for ImageNet images, 
# from https://github.com/pytorch/examples/blob/master/imagenet/main.py
norm = Normalize(mean=[0.485, 0.456, 0.406], std=[0.229, 0.224, 0.225])
```







## References

[1] Chollet. Deep learning with Python.

[2] Géron. Hands-On Machine Learning with Scikit-Learn & Tensorflow.