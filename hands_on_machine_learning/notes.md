# Notes



# Chapter 2. End-to-end Machine Learning Project

The author gives us a checkup list for addressing a machine learning project. In this section, the author builds a model for predicting housing prices in California as a example. In this scenario, these are our considerations:



| Step                                             | Considerations                                               |
| ------------------------------------------------ | ------------------------------------------------------------ |
| Look at the big picture                          | Here, we establish the business objective and what is the purpose of the model. With this information, can frame the problem, that is, we can determine whether is it a classification of regression problem; supervised of unsupervised; online or offline, etc. |
| Get the data                                     |                                                              |
| Discover and visualize the data to gain insights |                                                              |
| Prepare the data for Machine Learning algorithms |                                                              |
| Select a model and train it                      |                                                              |
| Fine-tune your model                             |                                                              |
| Present your solution                            |                                                              |
| Launch, monitor, and maintain your system        |                                                              |



In our example: 



| Step                                             | Considerations                                               |
| ------------------------------------------------ | ------------------------------------------------------------ |
| Look at the big picture                          | Our model should learn from data and be able to predict the median housing price in any district. Such a prediction will be used to feed another Machine learning system. There is a current solution: estimating prices manually by experts. |
| Get the data                                     |                                                              |
| Discover and visualize the data to gain insights |                                                              |
| Prepare the data for Machine Learning algorithms |                                                              |
| Select a model and train it                      |                                                              |
| Fine-tune your model                             |                                                              |
| Present your solution                            |                                                              |
| Launch, monitor, and maintain your system        |                                                              |





### Looking at the big picture

**Define the objective in business terms**

Building a model is probably not the end goal. This is important because it will determine how you frame the problem, what algorithms you will select (decision trees, neural networks, support vector machines), what performance measure you will use to evaluate your model (RMSE, MAE), and how much effort you should spend tweaking it.

> ....



**How will your solution be used?**

What are the current solutions/workarounds (if any)?

How should you frame this problem (supervised/unsupervised, online/offline, etc)

How should performance be measured?

Is the performance measure aligned with the business objective?

What would be the minimum performance needed to reach the business objective?

What are comparable problems? Can you reuse experience of tools?

Is human expertise available?

How would you solve the problem manually?

List the assumptions you (or others) have made so far.

Verify the assumptions if possible.

What is exactly the business objective?**  **What the current solution looks like (if any)?** If there exists a current solution, it will often give you a reference performance, as well as insights on how to solve the problem.





### Discover and visualize the data to gain insights

**Create a test set**

The author suggests a few ways to split the data into test and train sets (see pages 24, 25, and 51):

| Method              | Description                                                  | Disadvantages                                                |
| ------------------- | ------------------------------------------------------------ | ------------------------------------------------------------ |
| Random              | Take `test_ratio` of the samples randomly, typically `test_ratio=0.2`. | This method will generate a different test set every time. Over time, the machine learning algorithm will get to see the whole dataset, which is what you want to avoid. |
| Random + seed       | Use `np.random.seed()` before creating the splits.           | It will break if new instances are added into the dataset because the result of `np.random.permutation()` will be different if the number of instances is altered. |
| Hashing             | Assign a unique hash id to each instance. Then, put that instance in the test set if its hash is lower or equal to `test_ratio` = 20% of the maximum hash value. | The training set may not represent the overall population. This may lead to sampling bias, affecting the generalization of the machine learning algorithm. |
| Stratified sampling | The population (whole dataset) is divided into homogeneous subgroups called *strata*, and the right number of instances is sampled from each stratum to guarantee that the test set is representative of the overall population. | I am not sure it the sampled sets are affected when new instances are added into the dataset. |

Despite the method, it is important to split the data and set the test set aside for avoiding *data snooping* bias. All data analysis and exploration should be done on the test set.




## Different datasets

Can you spot the difference between this

```python
# my code
m_instances = 100
X = 2*np.random.rand(m_instances, 1) 
y = 4 * 3*X + np.random.rand(m_instances, 1)
```

and this?

```python
# code from Geron's book
import numpy as np
X = 2 * np.random.rand(100, 1)
y = 4 + 3 * X + np.random.randn(100, 1)
```

I found that my results from an example was not the same as in the book since my `y` had two mistakes (of differences):

- Notice that I employed `4 * 3 * X` instead of `4 + 3 * X` as Geron did.
- I used `np.random.rand` instead of `np.random.randn`. The first one samples random numbers from a uniform distribution, while the second one uses a normal distribution:

```
np.random.rand? 

Create an array of the given shape and populate it with
random samples from a uniform distribution
over ``[0, 1)``.
```

```
np.random.randn?

Return a sample (or samples) from the "standard normal" distribution.
```







## Questions

**Tail-heavy distributions**

In p. 59, the author mentions this:

> You noticed that some attributes have a tail-heavy distribution, so you may want to transform them (e.g., by computing their logarithm).

are there other techniques for dealing with tail-heavy distributions?







## Glosary



**Signal**

A piece of information fed to a Machine Learning system is often called a *signal* in reference to Shannon's information theory: you want a high signal/noise ratio (p. 35).



## TODO

- [ ] Hacer un `ImageDataGenerator` con `StratifiedKFold`
- [ ] Agregar mis notas sobre handling nans en un post, e indicar la diferencia en el indexado con `.iloc` y `.loc` 

````
isnan = df.isnan()

df.iloc[isnan]   # es un error
df.loc[isnan]    # si funciona

# diferentes formas de contabilizar nans por columna
# uso de missingno para visualizar nans
````



- [x] Investigar como interpretar la figura creada por:

```
import missingno as msno
cols = housing.columns[housing.isnull().any()].tolist()  # ['total_bedrooms', 'bedrooms_per_room']
msno.matrix(housing[cols]);
```



- [x] Funciones basicas de pandas

```
# un post con uno o dos comandos por cada ejemplo
# como crear un dataframe con dict y from csv
# como remover valores con drop

# drop
df = pd.DataFrame({"a": [1,2,3,4,5], "b": [10,20,30,40,50]})
df.drop(2)                       # drop row
df.drop("a", axis=1)             # drop column (axis is needed)
df.drop("a", axis=1).drop(2)     # drop both
```



- [x] agregar esto a post de indexado para revisar que funciona y que no



```
df = pd.DataFrame({"a": [1,2,3,4,5], "b": [10,20,30,40,50]})


df.iloc[0,0] = pd.np.nan
df.iloc[1,1] = pd.np.nan
df[df.isna().any(axis=1)]
#df.iloc[df.isna().any()];   # error


df.isna().any(axis=1)


df.loc[:, df.isna().any(axis=0)]

df["colname"]   	# p 60
df[["colname"]]  	# p 63
```

```
# este indexado parece funcionar para mostrar los nan de una columna

# nan values from original set
to_keep = housing.isna().any(axis=1)
housing["total_bedrooms"].loc[to_keep]		# ok
```

```python
# rows with nans
to_keep = housing.isna().any(axis=1)        # Serie
housing[to_keep]                            # DataFrame

housing_tr[to_keep]                  # it will throw 
                                     # IndexingError: Unalignable boolean Series provided as indexer
                                     
# fix:
# https://medium.com/dunder-data/selecting-subsets-of-data-in-pandas-39e811c81a0c


# esto tampoco funciona
housing_tr[housing.isna()]
```

La forma correcta (creo) de selecciona el mismo subset de rows de dos dataframes distintos es con un numpy array con indices:

```python
# get the indices of the rows with nan values in the training set
to_keep = housing.isna().any(axis=1)

# cast it as a numpy array
indices = to_keep.to_numpy()

# original data
housing[indices].tail()

# transformed data
housing_tr[indices].tail()

# ----- 
# geron usa esto
housing_tr.loc[sample_incomplete_rows.index.values]  # no me agrada, devuelve los resultados en otro orden
```

```python
# este indexado parece valido: usar un bool Series con .loc (con .iloc dara un error)
new_housing = imputer.fit_transform(housing_num)
new_housing_df = pd.DataFrame(new_housing, columns=housing_num.columns)

# nan values from original set
to_keep = housing.isna().any(axis=1)
housing["total_bedrooms"].loc[to_keep]
```

Este caso ilustra otra diff entre indexar con `loc` e `iloc`:

```python
# get the first five rows, as expected
some_data = housing.iloc[:5]
some_data.shape      # (5, 9)
```

```python
# esto toma todos los rows hasta el index 5 de housing.index
# en este caso, 5 se encuentra en la posicion 900 de housing.index
some_data = housing.loc[:5]   
some_data.shape      # (901, 9)
some_data.index[:2]  # Int64Index([17606, 18632], dtype='int64')
some_data.index[-2:] # Int64Index([9558, 5], dtype='int64')
housing.index[900]   # 5
```

```python
# Es importante conservar index=housing.index?
# We can convert the numpy array housing_extra_attribs to a dataframe as follows
housing_extra_attribs_df = pd.DataFrame(housing_extra_attribs,
                                        columns=list(housing.columns)+
                                        ["rooms_per_household", 
                                         "population_per_household", 
                                         "bedrooms_per_room"],
                                        index=housing.index)
```

```python
# ok
cols = ["params",
        "mean_test_score",
        "split0_test_score", "split0_train_score", 
        "split1_test_score", "split1_train_score",
        "split2_test_score", "split2_train_score",
        "split3_test_score", "split3_train_score",
        "split4_test_score", "split4_train_score",
       ]


df = pd.DataFrame(grid_search.cv_results_)[cols]

cols_fmt = {key: "{:.4f}" for key in cols}
cols_fmt["params"] = "{:}"  # ojo! no usar "{:10}" para formatear cadenas

df[cols[1:]] = np.sqrt(-df[cols[1:]])

df.head(1).style.format(cols_fmt)
```

