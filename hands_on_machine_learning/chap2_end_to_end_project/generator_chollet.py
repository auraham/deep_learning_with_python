# generator_chollet.py
from __future__ import print_function
import numpy as np
import matplotlib.pyplot as plt

def generator_chollet(data, lookback, delay, min_index, max_index, shuffle=False, batch_size=128, step=6, debug=False):
    
    
    if max_index is None:
        max_index = len(data) - delay - 1
    
    i = min_index + lookback
    
    # plot
    plt.figure()
    plt.plot(data[:, 1])
    
    plt.figure()
    plt.plot(data[:, 1], lw=4, c="#c0c0c0")
    
    while True:
        if shuffle:
            rows = np.random.randint(min_index + lookback, max_index, size=batch_size)
        else:
            if i + batch_size >= max_index:
                i = min_index + lookback
            rows = np.arange(0, min(i + batch_size, max_index))
            i += len(rows)
         
        # debug
        print("rows    ", rows)
        
        samples = np.zeros((len(rows), 
                           lookback // step,
                            data.shape[-1]))
        targets = np.zeros((len(rows), ))
        
        for j, row in enumerate(rows):
            indices = range(rows[j] - lookback, rows[j], step)
            samples[j] = data[indices]
            targets[j] = data[rows[j] + delay][1]
            
            
            
            y_sample = samples[j, :, 1]
            x_sample = indices
            plt.plot(x_sample, y_sample, marker="o", label="sample %d" % j)
        
            x_target = [rows[j] + delay]
            y_target = [targets[j]]
            plt.plot(x_target, y_target, marker="^", label="target %d" % j, ms=8)
        
            # debug
            print("x_target", x_target[0])
            
        plt.legend()
            
        yield samples, targets
        

if __name__ == "__main__":
    
    """
    # read dataset
    filepath = "/media/data/jena_climate_2009_2016.csv"
    lines = []

    with open(filepath, "r") as log:
        for line in log:
            lines.append(line.replace("\n", ""))

    header = lines[0].split(",")
    data = lines[1:]

    # casting data
    float_data = np.zeros((len(data), len(header)-1))

    for i, line in enumerate(data):
        values = [float(value) for value in line.split(",")[1:]]  # ignore the first column (Date Time)
        float_data[i, :] = values

    # normalizing data
    size = 200000
    mean = float_data[:size].mean(axis=0)
    std = float_data[:size].std(axis=0)
    float_data -= mean
    float_data /= std

    # train data
    train_data = float_data[:200000, :]
    """

    # mini test params
    #data = train_data[:20, :]
    data = np.genfromtxt("mini_data.txt")
    data = data[:, :2]      # presion and temp features only
    lookback = 4
    delay = 4
    min_index = 0
    max_index = data.shape[0]
    shuffle = False
    batch_size = 4
    step = 1
    debug = True

    # create generator
    train_gen = generator_chollet(data,
                          lookback,
                          delay,
                          min_index,
                          max_index,
                          shuffle,
                          batch_size,
                          step,
                          debug)

    for i in range(1):
        
        print("--- batch %d --- " % i)
        samples, targets = next(train_gen)
        print("shape   ", samples.shape)
        

    plt.show()
    
    """
    preguntas
    
    1. por que el batch es de 8 y no de 4?
    2. cuantos batches va a devolver? 2, 3?
    3. que pasa con el batch 4? reinicia el contador?
    """
