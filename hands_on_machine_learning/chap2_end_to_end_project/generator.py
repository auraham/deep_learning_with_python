import numpy as np


def generator(data, lookback, delay, min_index, max_index, shuffle=False, batch_size=128, step=6):
    
    if max_index is None:
        max_index = len(data) - delay - 1
    
    i = min_index + lookback
    
    while True:
        if shuffle:
            rows = np.random.randint(min_index + lookback, max_index, size=batch_size)
        else:
            if i + batch_size >= max_index:
                i = min_index + lookback
            rows = np.arange(0, min(i + batch_size, max_index))
            i += len(rows)
         
        # debug
        print("i        ", i)
        print("rows     ", rows)
        print("len(rows)", len(rows))
        
        samples = np.zeros((len(rows), 
                           lookback // step,
                            data.shape[-1]))
        targets = np.zeros((len(rows), ))
        
        for j, row in enumerate(rows):
            indices = range(rows[j] - lookback, rows[j], step)
            samples[j] = data[indices]
            targets[j] = data[rows[j] + delay][1]
        yield samples, targets

if __name__ == "__main__":
    
    # read dataset
    filepath = "/media/data/jena_climate_2009_2016.csv"
    lines = []

    with open(filepath, "r") as log:
        for line in log:
            lines.append(line.replace("\n", ""))

    header = lines[0].split(",")
    data = lines[1:]

    # casting data
    float_data = np.zeros((len(data), len(header)-1))

    for i, line in enumerate(data):
        values = [float(value) for value in line.split(",")[1:]]  # ignore the first column (Date Time)
        float_data[i, :] = values

    # normalizing data
    size = 200000
    mean = float_data[:size].mean(axis=0)
    std = float_data[:size].std(axis=0)
    float_data -= mean
    float_data /= std

    # create generator
    lookback = 144     # 1 dia  * 24 hrs * 6 timesteps
    step = 6
    delay = 72         # 0.5 dias * 24 hrs * 6 timesteps
    batch_size = 128

    train_gen = generator(float_data,
                          lookback=lookback,
                          delay=delay,
                          min_index=0,
                          max_index=200000,
                          shuffle=False,
                          step=step,
                          batch_size=batch_size)

    for step in range(1):
        samples, targets = next(train_gen)
        print(samples.shape)
