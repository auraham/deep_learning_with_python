- [x] usar t-sne
- [ ] remover el cat con menor densidad
- [ ] transformar `median_income` y `median_house_price` para eliminar la dispersion que se ve en los plots de pairs

my thoughts

- Use pandas for preprocessing, use numpy arrays for storing dataasets after preprocessing, this will ease the training and testing process (training with dataframes could lead to error when indexing, see my stratified cv).

- [x] crear un algoritmo evolutivo que optimize (mas bien, diseñe) un pipeline de scikit para transformar los datos de entrada (matriz de atributos `X` y posiblemente los labels `y`) con el fin de mejorar una metrica de prediccion (RMSE). Debera aplicar y evaluar una serie de tranformacions a cada atributo, seleccionar caracteristicas (columnas de `X`) y transformar el dataset para entrenamiento

```python
# dividir dataset
whole_dataset -> 
	(train_set_X, train_set_y), (test_set_X, test_set_y) = split(test=0.8) 

# transformar el dataset mediante un algoritmo evolutivo
# devolvera el pipeline que diseno y que conduce a una reduccion (mejora) del
# la metrica de desempeno, ie RMSE
# lo importante aqui es el pipeline, ya que lo usaremos para transformar test_set 
# y predecir los patrones de prueba con
# y_pred = lin_reg.predict(test_set_X)
improved_train, pipeline, lin_reg = EA(train_set_X, train_set_y, 
                                       metric="rmse", model=LinearRegression())

# tranformar test_set
test_set_X_t, test_set_y_t = pipeline(test_set_X, test_set_y)
y_pred = lin_reg.predict(test_set_X_t)

# podemos probar esta idea con RandomizedSearch antes de programar el evolutivo
# creo que keras tmb tiene una opcion para automatizar las pruebas con hyperparametros

# forst
```

