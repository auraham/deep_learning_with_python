# Binary Classification



**Definition**  In binary classification, our goal is to learn a classifier that can input an image [an example] represented by a  feature vector $x$ and predict whether the corresponding label $y$ is 1 or 0. In binary classification, the result is a discrete value output. Logistic regression is an algorithm for binary classification.



**Notation** Andrew Ng uses the following convention for representing training samples:

| Notation                           | Description                                                  |
| ---------------------------------- | ------------------------------------------------------------ |
| $n_x$                              | This is the number of features. In Python, it is called `n_features`. |
| $m$                                | This is the number of training examples. Other notations: $m_{train}$ for number of training examples, $m_{test}$ for number of test examples. In Python, it is called `m`. |
| $(x, y )$                          | This is a single training (testing) example, where $x \in \mathbb{R}^{n_x}$ is a vector and $y \in \{0, 1\}$ is an integer (label or target). |
| $x^{(i)} \in \mathbb{R}^{n_x}$     | This is the $i$-th example represented as a column vector. In NumPy, the shape of $x^{(i )}$ is `x_i.shape`, that is, `[n_features, 1]`. |
| $X \in \mathbb{R}^{ n_x \times m}$ | This is the input matrix. This matrix has $m$ columns and $n_x$ rows. Unlike other courses, here each column is a training (testing) example. This convention ($n_x \times m$) will make the implementation of neural networks much easier. In NumPy, the shape of this matrix, `X.shape`, is `[n_features, m]`. |
| $Y \in \mathbb{R}^{1 \times m}$    | This is the matrix of labels (targets). Again, unlike other courses, here we follow another convention for storing labels, that is, $(1 \times m)$. In Numpy, the shape of this vector, `y.shape` is `[1, m]`. |



[colocar aqui imagen de matrices X y]



## Logistic Regression

**Definition** Logistic regression is an algorithm for binary classification. Logistic regression is a learning algorithm that you use when the output labels $Y$ in a supervised learning problem are all either zero of one (i.e., binary classification problems). The goal of logistic regression is to minimize the error between its predictions ($\hat{y}$) and the true targets ($y$).



**Example: cat vs. not-cat** Given an image represented by the feature vector $x$, the algorithm evaluates the probability of a cat being in the image ($\hat{ y }$):
$$
\text{Given } x, \text{ obtain } \hat{y} = P(y = 1 | x), \text{ where } 0 \leq \hat{y} \leq 1
$$

$$
\text{Parameters: } w \in \mathbb{R}^{n_x} \text{ and } b \in R
$$

$$
\text{Output: } \hat{y} = \sigma(w^Tx + b)
$$

The binary classification problem can be defined as follows. We want to find $w$ and $b$ such that the predicted label ($\hat{y}$) is close to the real label ($y$) of each given example ($x$), that is $\hat{y } \approx y$. To find the parameters $w$ and $b$ we need to define a **loss function** to measure the closeness between $\hat{y}$ and $y$, for example, $L(\hat{y}, y)$. The loss function allows us to measure the classification error. In other words, if $L(\hat{y}, y )$ is small, then the classification error is small, meaning that the predicted label $\hat{y}$ is close to the ground-truth label $y$.



**Inputs and parameters of logistic regression**

| Item                                      | Description                                                  |
| ----------------------------------------- | ------------------------------------------------------------ |
| $x \in \mathbb{R}^{n_x }$                 | This is the input vector, where $n_x$ is the number of features. Here, it represents an input image. |
| $ y  \in \{ 0, 1\} $                      | The label of the input vector $x$.                           |
| $w \in \mathbb{R}^{n_x}$                  | The weights, where $n_x$ is the number of features. There is a weight for each feature of $x$. |
| $b \in R$                                 | The threshold (also called bias).                            |
| $z = w^Tx + b$                            | This is the linear combination of $x$ and $w$ (dot product). Remember, both $x$ and $w$ are columns. Thus, we need to transpose $w$ before computing the dot product. |
| $\hat{y} = \sigma(w^{T}x+b)  = \sigma(z)$ | The predicted label of the input vector $x$. Here, $\sigma(\cdot{ })$ is a non-linear function. |
| $s = \sigma(z) = \frac{1}{1 + e^{-z}}$    | This is the **logistic sigmoid function**.                   |



$(w^Tx + b)$ is a linear function $(ax + b)$, but since we are looking for a probability constraint between 0 and 1, the logistic sigmoid function is used. 



Be aware of the term *sigmoid*. In machine learning, we often use the term *sigmoid* to refer to the *logistic sigmoid function*. It turns out a sigmoid function is a mathematical function which has a S-shaped curve.

[imagenes de los diferentes tipos de sigmoid]



From [DeepAI](https://deepai.org/machine-learning-glossary-and-terms/sigmoid-function):

> A Sigmoid function is a mathematical function which has a characteristic S-shaped curve. There are a number of common sigmoid functions, such as the *logistic function*, the *hyperbolic tangent*, and the *arctangent*. 
>
> **In machine learning, the term sigmoid function is normally used to refer specifically to the logistic function, also called the logistic sigmoid function.**
>
> All sigmoid functions have the property that they map the entire number line into a small range such as between 0 and 1, or -1 and 1, so one use of a sigmoid function is to convert a real value into one that can be interpreted as a probability.



We can use sigmoid functions as activation functions in neural networks and for converting a real number (e.g., $z = w^Tx + b$) into a probability score. From [DeepAI](https://deepai.org/machine-learning-glossary-and-terms/sigmoid-function):

> Sigmoid functions have become popular in deep learning because they can be used as an **activation function** in an artificial neural network. They were inspired by the activation potential in biological neural networks.
> 
> Sigmoid functions are also useful for many machine learning applications where a real number needs to be converted to a probability. A sigmoid function placed as the last layer of a machine learning model can serve **to convert the model's output into a probability score**, which can be easier to work with and interpret.



Worth noting that **logistic regression** is a modification of **linear regression** for binary classification. From [DeepAI](https://deepai.org/machine-learning-glossary-and-terms/sigmoid-function):

> Sigmoid functions are an important part of a logistic regression model. Logistic regression is a modification of linear regression for two-class classification, and converts one or more real-valued inputs into a probability, such as the probability that a customer will purchase a product. The final stage of a logistic regression model is often set to the logistic function, which allows the model to output a probability.



From [Pumperla]

> The sigmoid maps real values to a range of [0, 1].
>
> You applied an activation function, the sigmoid $y = \sigma(z)$, to get output neurons $y$. The outcome of applying $\sigma$ tells you how much $y$ activates.
>
> The sigmoid function is an activation function that takes a vector of real-valued neurons and *activates* them so that they're mapped to the range [0, 1]. You interpret values close to 1 as activating. 



The logistic sigmoid function is defined as follows:
$$
\sigma(z) = \frac{1}{1 + e^{-z}}
$$
Alternatively, it can also be defined as:
$$
\sigma(z) = \frac{e^{z}}{e^{z} + 1}
$$



![](img/logistic_sigmoid.png)



Some additional notes about the logistic sigmoid function $ \sigma(z)$:

- The values of $z$ are in the range [$-\infty$, $+\infty$]. **Intuition**: the linear combination (dot product) $w^Tx + b$ can return any real number, it can be a large negative (positive) number.
- If $z$ is a large positive number, then $\sigma(z)$ is close to 1.
- If $z$ is a large negative number, then $\sigma(z)$ is close to 0.
- If $z$ is 0, then $\sigma(z)$ is 0.5.



**Code**

```python
import numpy as np
import matplotlib.pyplot as plt

def sigma(z):
    return 1 / (1 + np.exp(-z))

# plot
x = np.linspace(-10, 10, 100)
y = sigma(x)
fig, ax = plt.subplots(1,1, figsize=(10, 4))
ax.plot(x, y)
```





**Recommended Lectures**

- [What is the Sigmoid Function?](https://deepai.org/machine-learning-glossary-and-terms/sigmoid-function)
- [Linear Regression vs. Logistic Regression](https://www.dummies.com/programming/big-data/data-science/linear-regression-vs-logistic-regression/)





## Loss Function and Cost Function

To find the parameters $w$ and $b$, we need to define a **loss function** and a **cost function**. 

Given an example $x^{(i)}$, we want to find $w$ and $b$ such that the predicted label $\hat{y}^{(i)}$ is close to the real label $y^{(i)}$. 

$$
\text{Prediction: } \hat{y}^{(i)} = \sigma(w^Tx^{(i)} + b) = \sigma(z^{(i)})
$$

$$
\text{Logistic sigmoid: } \sigma(z^{(i)}) = \frac{1}{1 + e^{{-z}^{(i)}}}
$$

$$
\text{Given a dataset } \{(x^{(i)}, y^{(i)}), \dots, (x^{(m)}, y^{(m)})\}, \text{ find } \hat{y}^{(i)} \approx y^{(i)} 
$$

Here we introduce the superscript $(i)$ to refer to the $i$-th example in the dataset.

We use the **loss function** $L(\hat{y}^{(i)}, y^{(i)})$ to measure the discrepancy between the prediction $\hat{y}^{(i) }$ and the desired output $y^{(i) }$. In other words, **the loss function computes the error for a single training example**. 
$$
\text{Loss (error) function: } L(\hat{y}^{(i)}, y^{(i)})
$$

The **cost function** $J(w, b)$ is the **average of the loss function on the entire training set**. We want to find the parameters $w$ and $b$ that minimize the overall cost function:
$$
\text{Minimize } J(w, b) = \frac{1}{m}\sum_{i=1}^{m} L(\hat{y}^{(i)}, y^{(i)})
$$

Now that we know the purpose of the loss function and the cost function, let's discussion two loss functions: the squared error and the binary cross-entropy:
$$
\text{Squared Error } L(\hat{y}^{(i)}, y^{(i)}) = \frac{1}{2}(\hat{y}^{(i)} - y^{(i)})^{2}
$$

$$
\text{Binary Cross-Entropy } L(\hat{y}^{(i)}, y^{(i)}) = -\left[y^{(i)} \log (\hat{y}^{(i)}) + (1 - y^{(i)}) \log(1-\hat{y}^{(i)})\right]
$$

In the lecture, Andrew Ng mentions that the squared error is a reasonable choice as a loss function. Although, he later mentioned that if you use that function, the optimization problem (i.e., minimizing the squared error) becomes non-convex, so you end up with an optimization problem with multiple local optima. As a result, the optimizer (e.g., gradient descent) will have difficulty to find good parameters $w$ and $b$. In logistic regression, we actually define a different loss function that plays a similar role as the squared error. Such a function is called *categorical cross-entropy loss function* [Pumperla p.139], *negative log-likelihood cost* [Burkov Chap. 8.6] or just *cross-entropy*. Since we have a binary classification problem, we call it *binary cross-entropy*:

> So in logistic regression, we will actually define a different loss function that plays a similar role as squared error, that will give us an optimization problem that is convex [which] becomes much easier to optimize. 

In both loss functions, squared error and cross-entropy, we want the loss value (i.e., the error) to be as small as possible. Below, we outline the intuition of the cross-entropy.



[notas en goodnotes sobre la intuicion del log loss]



The full formula of the cost function is given below:

$$
\text{Minimize } J(w, b) = \frac{1}{m}\sum_{i=1}^{m} L(\hat{y}^{(i)}, y^{(i)}) = - \frac{1}{m} \sum_{i=1}^{m} \left[ y^{(i)} \log (\hat{y}^{(i)}) + (1 - y^{(i)}) \log(1-\hat{y}^{(i)}) \right]
$$

**Recommended Lectures**

- [Pumperla] Chapter 5.4.2 introduces the mean squared error (MSE) function. Section 6.5.2 discusses why MSE is not suitable for classification problems and explains why cross-entropy is a best choice.



---

**Quiz** What is the difference between the cost function and the loss function for logistic regression?

**Answer** The loss function computes the error for a single training example; the cost function is the average of the loss function of the entire training set.

----

**Note** Some authors use $L(y^{(i)},\hat{y}^{(i)}) $ to denote the loss function, where the first parameter is the real label and the second parameter is the predicted label. Frameworks like Keras and Scikit-learn use this convention as well. Andrew Ng uses $L(\hat{y}^{(i)}, y^{(i)}) $ instead. 

Also, Andrew Ng distinguishes between the loss function and the cost function. In other courses or frameworks, both functions are simply called the loss function. For example, in [Pumperla]

---



## Gradient Descent

The loss function measures how well you are doing on a single training example, whereas the cost function measures how well the parameters $w$ and $b$ are doing on the entire training set. Now, let's talk about how you can use the gradient descent algorithm to train (i.e., to learn) the parameters $w$ and $b$ on the training set. To recap, this is the cost function:

$$
\text{Minimize } J(w, b) = \frac{1}{m}\sum_{i=1}^{m} L(\hat{y}^{(i)}, y^{(i)}) = - \frac{1}{m} 
\sum_{i=1}^{m} \left[ y^{(i)} \log (\hat{y}^{(i)}) + (1 - y^{(i)}) \log(1-\hat{y}^{(i)}) \right]
$$

The cost function measures how well the parameters $w$ and $b$ are doing on the entire training set. So in order to learn the parameters $w$ and $b$ it seems natural that we want to find $w$ and $b$ that make the cost function $J(w, b) $ as small as possible.



[aqui seria bueno colocar una imagen del espacio de las variables para ver la superficie convexa e ilustrar la caminata de gradient descent]



Andrew mentions that this particular cost function is convex, making it a suitable choice for gradient descent:

>It turns out that this cost function $J(w,  b) $ is a convex function. [...] So the fact that our cost function $J(w, b) $ as defined here is convex is one of the huge reasons why we use this particular cost function for logistic regression.

Other cost functions have multiple local optimum points (i.e., they are non-convex cost functions). Unlike them, our cost function $J(w, b)$ is a convex function, meaning that it has a single (i.e., global) optimum point, and it is located in the bottom of the bowl. In order to reach that point, we use gradient descent.

In gradient descent, we first initialize $w$ and $b$ to define an initial point. We then take a step in the steepest downhill direction. After this, we will be in another point. [mejorar esta descripcion de gradient descent]




$$
\text{Repeat } \{ \\
\;\;\;\;\;\;\;\;\;\;\;\;\;\;\;\;\;\;\;\;\;\;\;\; w := w - \alpha \frac{dJ(w)}{dw} \\
\}
$$




[colocar el ejemplo de gradient descent en 2D para entender cómo converge hacia el centro del bowl desde la izquierda o desde la derecha]



## Derivatives

[aqui habla de los nudges] con el ejemplo $f(a) = 3a$

lo importante de estos ejemplos es mostrar que a medida que el nudge se hace mas pequeno (0.001 .... 0.0001) la pendiente tiene a 3

## More Derivative Examples

[aqui habla de $f(a)=a^2$, $f(a) = a^3$, $f(a) = \log(a)$ ]



## Computation Graph

[aqui explica el computation graph de $J(a,b,c) = 3(a+bc)$] Para explicar esta parte, dibuja el computation graph.



[agregar notaas adicionales de como otros autores dibujan el computation graph]







## Derivatives with a Computation Graph

aqui explica la intuicion de la regla de la cadena:

aqui explica como un cambio pequeno en una variable afecta el cambio en una variable intermedia y como a su vez afecta la salida de la funcion final





## Logistic Regression Gradient Descent

aqui explica como calcular las derivadas parciales usando el computation graph (de un solo example/training example)

este video es importante porque explica como calcular $dw$, $db$ y $dz$ ($dz$ es necesario en la regla de la cadena)



## Gradient Descent on $m$ examples



aqui muestra por primera vez el loop de los m examples para calcular dw y db. Ambos dw y db son acumuladores. Al termino del loop, calcula dw como un promedio:

```
J /= m
dw1 /= m
dw2 /= m
db /= m
```

Finalmente, actualiza los parametros:

```
w1 := w1 - alpha * dw1
w2 := w2 - alpha * dw2
b := b - alpha * db
```



Luego, presenta la idea de vectorizar el calculo de variables.



**Nota** no habia notado pero Andrew no usa $1/2m$ en el cost function como generalmente se hace en algunos libros. Cuando usamos mean squared error (MSE) recuerda que la diferencia esta elevada al cuadrado, ie $(\hat{y} - y)^2$. Cuando se deriva el loss, podemos eliminar el 2 al usar el factor $1/2m$. Sin embargo, en este curso hablamos de clasificacion, no regression. En regression conviene usar MSE. En clasificacion conviene usar crossentropy. Como crossentropy no eleva al cuadrado la diferencia $\hat{y} - y$, no es necesario incluir un 2 en el factor $1/2m $.

 

## Derivation of $\partial L/\partial z$







## More Vectorization Examples

Aqui presenta el algoritmo de logistic regresion en una version limpia y secuencial.

Luego, reemplaza el loop interno con vectorizacion



## Vectorizing Logistic Regression

Aqui explica como quitar el segundo loop (creo) para hacer operaciones matricial en lugar de vectoriales, ie dA, dZ, etc.



## Vectorizing Logistic Regression's Gradient Output

Aqui reemplaza los dos loops



## Broadcasting in Python

aqui explica el ejemplo de las calorias para calcular el promedio por col



seria bueno incluir un ejemplo en donde se explique la diff entre:

```
a = (3,3)
b = (3, )
c = (1, 3)
d = (3, 1)

cual es la salida de 
a+b
a+c
a+d
```



## A note on Python/NumPy Vectors

aqui dice que no hay que usar `(n, )` vectors, mejor usa `(n, 1)` o `(1, n)`



## Explanation of logistic regression cost function

explica algunos detalles de la funcion de costo (por que tiene esa forma)



## References

- [Pumperla]