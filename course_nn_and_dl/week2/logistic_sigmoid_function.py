# logistic_sigmoid_function.py
import numpy as np
import matplotlib.pyplot as plt

def sigma(z):
    return 1 / (1 + np.exp(-z))

x = np.linspace(-10, 10, 100)
y = sigma(x)

fig, ax = plt.subplots(1,1, figsize=(10, 4))
ax.plot(x, y)
ax.set_title("Logistic sigmoid function")
ax.set_ylim(-0.05, 1.05)
ax.set_xlabel("$z$", fontsize=16)
ax.set_ylabel("$\sigma(z)$", fontsize=16)
ax.set_yticks(np.linspace(0, 1, 11))
ax.set_xticks(np.arange(-10, 11))
ax.grid()
plt.subplots_adjust(bottom=0.2)
plt.savefig("img/logistic_sigmoid.png")
plt.show()
