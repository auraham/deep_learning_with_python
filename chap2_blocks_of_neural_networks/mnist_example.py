# mnist_example.py
from __future__ import print_function
from keras.datasets import mnist
from keras import models
from keras import layers
from keras.utils import to_categorical
import matplotlib.pyplot as plt

def print_matrix(vector):
    
    for row in vector:
        print(" ".join(["%.4f" % val for val in row]))

if __name__ == "__main__":
    
    # load dataset
    (train_images, train_labels), (test_images, test_labels) = mnist.load_data()
    
    # data preprocessing
    train_images = train_images.reshape((60000, 28*28))
    train_images = train_images.astype("float32") / 255
    
    test_images = test_images.reshape((10000, 28*28))
    test_images = test_images.astype("float32") / 255
    
    # prepare labels
    train_labels = to_categorical(train_labels)
    test_labels = to_categorical(test_labels)
    
    # network arquitecture
    network = models.Sequential()
    network.add(layers.Dense(512, activation="relu", input_shape=(28*28, )))
    network.add(layers.Dense(10, activation="softmax"))
    
    # compilation
    network.compile(optimizer="rmsprop",
                    loss="categorical_crossentropy",
                    metrics=["accuracy"],                
        )


    # train
    network.fit(train_images, train_labels, epochs=5, batch_size=128)
    
    # evaluation
    test_loss, test_acc = network.evaluate(test_images, test_labels)
    print("test_loss: %.4f" % test_loss)
    print("test_acc:  %.4f" % test_acc)
    
    # classification
    sample_id = 4           # choose a sample
    sample = test_images[[sample_id]]

    pred = network.predict(sample)
    
    # real label
    real_label = test_labels[sample_id].argmax()

    # predicted label
    pred_label = pred[0].argmax()

    print("pred: %d, real: %d" % (pred_label, real_label))


    # plot sample
    digit = sample[0].reshape((28, 28))       # reshape
    plt.imshow(digit, cmap=plt.cm.binary)
    plt.show()
    

