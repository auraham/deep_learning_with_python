# build_model.py
from __future__ import print_function
import numpy as np
from keras import layers
from keras import models
from keras import optimizers
from keras.preprocessing.image import ImageDataGenerator
import matplotlib.pyplot as plt

if __name__ == "__main__":
    
    input_h = 150       # height
    input_w = 150       # width
    input_c = 3         # number of channels
    save_model = True   # flag
    
    # --- paths ----
    # train (cats):       /media/data/dogs_vs_cats_small/train/cats
    # train (dogs):       /media/data/dogs_vs_cats_small/train/dogs
    # validation (cats):  /media/data/dogs_vs_cats_small/validation/cats
    # validation (dogs):  /media/data/dogs_vs_cats_small/validation/dogs
    # test (cats):        /media/data/dogs_vs_cats_small/test/cats
    # test (dogs):        /media/data/dogs_vs_cats_small/test/dogs

    # --- data preprocessing ---
    train_datagen = ImageDataGenerator(rescale=1./255)      # rescale images by 1/255 
    test_datagen  = ImageDataGenerator(rescale=1./255)      # so the values are in the range [0, 1]
    
    
    train_dir = "/media/data/dogs_vs_cats_small/train"
    train_generator = train_datagen.flow_from_directory(train_dir,                          # path of images
                                                        target_size=(input_h, input_w),     # image size
                                                        batch_size=20,                      # number of images per batch
                                                        class_mode="binary"                 # because you use binary_crossentropy, you need binary labels
                                                        )
    
    validation_dir = "/media/data/dogs_vs_cats_small/validation"
    validation_generator = train_datagen.flow_from_directory(validation_dir,                # path of images
                                                        target_size=(input_h, input_w),     # image size
                                                        batch_size=20,                      # number of images per batch
                                                        class_mode="binary"                 # because you use binary_crossentropy, you need binary labels
                                                        )
    
    # test
    for data_batch, labels_batch in train_generator:
        print("data batch shape:   ", data_batch.shape)
        print("labels batch shape: ", labels_batch.shape)
        break
    
    # --- create model ----
    model = models.Sequential()
    
    # four convolution+maxpooling layers
    model.add(layers.Conv2D(32, (3,3), activation="relu", input_shape=(input_h, input_w, input_c)))
    model.add(layers.MaxPooling2D((2,2)))
    
    model.add(layers.Conv2D(64, (3,3), activation="relu"))
    model.add(layers.MaxPooling2D((2,2)))
    
    model.add(layers.Conv2D(128, (3,3), activation="relu"))
    model.add(layers.MaxPooling2D((2,2)))
    
    model.add(layers.Conv2D(128, (3,3), activation="relu"))
    model.add(layers.MaxPooling2D((2,2)))
    
    # flatten layer
    model.add(layers.Flatten())
    
    # classifier
    model.add(layers.Dense(512, activation="relu"))
    model.add(layers.Dense(1, activation="sigmoid"))
    
    # look the model
    model.summary()
    
    # --- compile model ---
    model.compile(optimizer=optimizers.RMSprop(lr=1e-4),
                loss="binary_crossentropy",
                metrics=["acc"])
                
                
    # --- train model ---
    history = model.fit_generator(
                    train_generator,
                    steps_per_epoch=100,
                    epochs=30,
                    validation_data=validation_generator,
                    validation_steps=50)
    
    # --- save model ----
    if save_model:
        filename = "dogs_vs_cats_small_v1.h5"
        model.save(filename)
        print("saving model", filename)
    
    
    # --- stats ---
    acc = history.history["acc"]
    val_acc = history.history["val_acc"]
    loss = history.history["loss"]
    val_loss = history.history["val_loss"]
    
    epochs = range(1, len(acc)+1)
    
    plt.plot(epochs, acc, "bo", label="Training acc")
    plt.plot(epochs, val_acc, "b", label="Validation acc")
    plt.title("Training and validation accuracy")
    plt.legend()

    plt.plot(epochs, loss, "bo", label="Training loss")
    plt.plot(epochs, val_loss, "b", label="Validation loss")
    plt.title("Training and validation loss")
    plt.legend()
    
    plt.show()
