# clase





### todo

- [ ] agregar notebook con activaciones de feature maps
- [x] agregar figuras con la arquitectura de la red
- [ ] revisar como los dataimagegenerators generan las transformaciones de las imagenes
- [ ] pasar el tercer notebook a un script python para ejecutarlo en `deepsea` y guardar la red en el repositorio (aunque aun asi seguro ocupare mas memoria en mi laptop, creo que sera mejor instalar jupyter en `deepsea` y conectarme de forma remota)
- [ ] slide o notebook con resources/ligas a referencias
- [ ] agregar un ejemplo/section en cada notebook (los primeros tres) para revisar como clasifica algunas imagenes del test set (o hacer un solo notebook `test_models.ipynb` en donde carge las redes y las evaluae con algunas imagenes del test set)
- [ ] empezar con un notebook que muestre las imagenes para entender primero la tarea de clasificacion (`classify.ipynb`)



### material (notebooks)

- [ ] `build_model_v1`
- [ ] `build_model_v2`
- [ ] `build_model_v3`
  - [ ] agregar dropout
- [ ] `feature_maps`
- [ ] `generative_deep_learning`
- [ ] `neural_style_transfer`
- [ ] `intro` con conceptos y referencias