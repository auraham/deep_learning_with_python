# get_dataset.py
from __future__ import print_function
import os, shutil

if __name__ == "__main__":
    
    # path where the original dataset was uncompressed
    original_dataset_dir = "/media/data/kaggle_original_data"
    
    # path where we will store our smaller dataset
    base_dir = "/media/data/dogs_vs_cats_small"
    os.mkdir(base_dir)
    
    # directories for our training, validation, and test splits
    train_dir = os.path.join(base_dir, "train")
    validation_dir = os.path.join(base_dir, "validation")
    test_dir = os.path.join(base_dir, "test")
    os.mkdir(train_dir)
    os.mkdir(validation_dir)
    os.mkdir(test_dir)
    
    # directory with our training cat pictures
    train_cats_dir = os.path.join(train_dir, "cats")
    os.mkdir(train_cats_dir)
    
    # directory with our training dog pictures
    train_dogs_dir = os.path.join(train_dir, "dogs")
    os.mkdir(train_dogs_dir)
    
    # directory with our validation cat pictures
    validation_cats_dir = os.path.join(validation_dir, "cats")
    os.mkdir(validation_cats_dir)
    
    # directory with our validation dog pictures
    validation_dogs_dir = os.path.join(validation_dir, "dogs")
    os.mkdir(validation_dogs_dir)
    
    # directory with our test cat pictures
    test_cats_dir = os.path.join(test_dir, "cats")
    os.mkdir(test_cats_dir)
    
    # directory with our test dog pictures
    test_dogs_dir = os.path.join(test_dir, "dogs")
    os.mkdir(test_dogs_dir)
    
    # copy first 1000 cat images to train_cats_dir
    fnames = ["cat.{}.jpg".format(i) for i in range(1000)]
    for fname in fnames:
        src = os.path.join(original_dataset_dir, fname)
        dst = os.path.join(train_cats_dir, fname)
        shutil.copyfile(src, dst)
        
    # copy next 500 cat images to validation_cats_dir
    fnames = ["cat.{}.jpg".format(i) for i in range(1000, 1500)]
    for fname in fnames:
        src = os.path.join(original_dataset_dir, fname)
        dst = os.path.join(validation_cats_dir, fname)
        shutil.copyfile(src, dst)
        
    # copy next 500 cat images to test_cats_dir
    fnames = ["cat.{}.jpg".format(i) for i in range(1500, 2000)]
    for fname in fnames:
        src = os.path.join(original_dataset_dir, fname)
        dst = os.path.join(test_cats_dir, fname)
        shutil.copyfile(src, dst)
        
    # copy first 1000 dog images to train_cats_dir
    fnames = ["dog.{}.jpg".format(i) for i in range(1000)]
    for fname in fnames:
        src = os.path.join(original_dataset_dir, fname)
        dst = os.path.join(train_dogs_dir, fname)
        shutil.copyfile(src, dst)
        
    # copy next 500 dog images to validation_cats_dir
    fnames = ["dog.{}.jpg".format(i) for i in range(1000, 1500)]
    for fname in fnames:
        src = os.path.join(original_dataset_dir, fname)
        dst = os.path.join(validation_dogs_dir, fname)
        shutil.copyfile(src, dst)
        
    # copy next 500 dog images to test_cats_dir
    fnames = ["dog.{}.jpg".format(i) for i in range(1500, 2000)]
    for fname in fnames:
        src = os.path.join(original_dataset_dir, fname)
        dst = os.path.join(test_dogs_dir, fname)
        shutil.copyfile(src, dst)
        
    # sanity check
    print("total training cat images:", len(os.listdir(train_cats_dir)))    
    print("total training dog images:", len(os.listdir(train_dogs_dir)))  
      
    print("total validation cat images:", len(os.listdir(validation_cats_dir)))    
    print("total validation dog images:", len(os.listdir(validation_dogs_dir)))    
    
    print("total test cat images:", len(os.listdir(test_cats_dir)))    
    print("total test dog images:", len(os.listdir(test_dogs_dir)))    
    
