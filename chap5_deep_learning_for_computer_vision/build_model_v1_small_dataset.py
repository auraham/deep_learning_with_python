# build_model_v1_small_dataset.py
from __future__ import print_function
import numpy as np
from keras import layers
from keras import models
from keras import optimizers
from keras.preprocessing.image import ImageDataGenerator
import matplotlib.pyplot as plt

if __name__ == "__main__":
    
    input_h = 150       # height
    input_w = 150       # width
    input_c = 3         # number of channels
    save_model = True   # flag

    # --- paths ---
    # train (cats):       /media/data/dogs_vs_cats_small/train/cats
    # train (dogs):       /media/data/dogs_vs_cats_small/train/dogs
    # validation (cats):  /media/data/dogs_vs_cats_small/validation/cats
    # validation (dogs):  /media/data/dogs_vs_cats_small/validation/dogs
    # test (cats):        /media/data/dogs_vs_cats_small/test/cats
    # test (dogs):        /media/data/dogs_vs_cats_small/test/dogs

    # --- data preprocessing ---
    train_datagen = ImageDataGenerator(rescale=1./255)          # rescale all images by 1/255
    validation_datagen = ImageDataGenerator(rescale=1./255)
    
    train_path = "/media/data/dogs_vs_cats_small/train"
    train_generator = train_datagen.flow_from_directory(train_path,
                                                        target_size=(input_h, input_w),
                                                        batch_size=20,
                                                        class_mode="binary"
                                                        )
                                                        
    validation_path = "/media/data/dogs_vs_cats_small/validation"
    validation_generator = validation_datagen.flow_from_directory(validation_path,
                                                                target_size=(input_h, input_w),
                                                                batch_size=20,
                                                                class_mode="binary"
                                                                )
                                                                
    # check generator
    for data_batch, labels_batch in train_generator:
        print("data batch shape:  ", data_batch.shape)
        print("labels batch shape:", labels_batch.shape)
        break
    
    
    # check labels
    print("train mapping:", train_generator.class_indices)
    print("validation mapping:", validation_generator.class_indices)
    
    # --- build model ---
    model = models.Sequential()
    
    model.add(layers.Conv2D(32, (3, 3), activation="relu", input_shape=(input_h, input_w, input_c)))
    model.add(layers.MaxPooling2D((2,2)))
    
    model.add(layers.Conv2D(64, (3,3), activation="relu"))
    model.add(layers.MaxPooling2D((2,2)))
    
    model.add(layers.Conv2D(128, (3,3), activation="relu"))
    model.add(layers.MaxPooling2D((2,2)))
    
    model.add(layers.Conv2D(128, (3,3), activation="relu"))
    model.add(layers.MaxPooling2D((2,2)))
    
    model.add(layers.Flatten())
    
    model.add(layers.Dense(512, activation="relu"))
    model.add(layers.Dense(1, activation="sigmoid"))
    
    
    # check the model
    model.summary()
    
    # --- compile the model ---
    model.compile(loss="binary_crossentropy",
                optimizer=optimizers.RMSprop(lr=1e-4),
                metrics=["acc"])
                
    # --- trainig ---
    history = model.fit(train_generator,
            steps_per_epoch=100,
            epochs=30,
            validation_data=validation_generator,
            validation_steps=50
            )
            
    # --- save model ---
    model.save("dogs_vs_cats_v1.h5")
    
    # --- plotting ---
    
    acc = history.history["acc"]
    val_acc = history.history["val_acc"]
    loss = history.history["loss"]
    val_loss = history.history["val_loss"]
    
    epochs = range(1, len(acc)+1)
    
    plt.plot(epochs, acc, "bo", label="Training Acc")
    plt.plot(epochs, val_acc, "b", label="Validation Acc")
    plt.title("Training and validation accuracy")
    plt.legend()
    
    plt.figure()
    
    plt.plot(epochs, loss, "bo", label="Training Loss")
    plt.plot(epochs, val_loss, "b", label="Validation Loss")
    plt.title("Training and validation loss")
    plt.legend()
    
    plt.show()
                                            
    
