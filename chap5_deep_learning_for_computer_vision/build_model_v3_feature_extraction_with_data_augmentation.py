# build_model_v3_feature_extraction_with_data_augmentation.py
import os
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.pyplot import rcParams
from keras import models
from keras import layers
from keras import optimizers
from keras.preprocessing.image import ImageDataGenerator
from keras.applications import VGG16
from keras.callbacks import ModelCheckpoint

def load_rcparams(figsize=None):
    """
    Load a custom rcParams dictionary
    """
    
    rcParams['axes.titlesize']  = 14            # title
    rcParams['axes.labelsize']  = 10            # $f_i$ labels
    rcParams['xtick.color']     = "#474747"     # ticks gray color
    rcParams['ytick.color']     = "#474747"     # ticks gray color
    rcParams['xtick.labelsize'] = 10            # ticks size
    rcParams['ytick.labelsize'] = 10            # ticks size
    rcParams['legend.fontsize'] = 12            # legend
    rcParams['legend.fontsize'] = 12            # legend

    if isinstance(figsize, tuple):
        rcParams['figure.figsize'] = figsize
        
        
base_dir = "/media/data/dogs_vs_cats_small"                         # local
base_dir = "/home/auraham/datasets/dogs_vs_cats_small"              # server
train_dir = os.path.join(base_dir, "train")
validation_dir = os.path.join(base_dir, "validation")
test_dir = os.path.join(base_dir, "test")


# load convolutional base
conv_base = VGG16(
            weights="imagenet",
            include_top=False,           # include or not the densely connected classifier on top of the network
            input_shape=(150, 150, 3)    # shape of the image tensors that you will feed to the network
            )
            
# Listing 5.20 Adding a densely connected classifier on top of the convolutional base
model = models.Sequential()

model.add(conv_base)

# feature map shape: 
model.add(layers.Flatten())                            

# classifier
model.add(layers.Dense(256, activation="relu"))        # 256 hidden neurons
model.add(layers.Dense(1, activation="sigmoid"))       # 1 neuron


print("This is the number of trainable weights before freezing the conv base:", len(model.trainable_weights))

# freeze layers
conv_base.trainable = False

print("This is the number of trainable weights after freezing the conv base:  ", len(model.trainable_weights))



# Listing 5.21 Training the model end to end with frozen convolutional base
train_datagen = ImageDataGenerator(
                    rescale=1./255,             # we multiply the data by the value provided (after applying all other transformations)
                    rotation_range=40,          # int, degree range for random rotations
                    width_shift_range=0.2,      # float, fraction of total width (other types are also available)
                    height_shift_range=0.2,     # float, fraction of total width (other types are also available)
                    shear_range=0.2,            # float, shear Intensity (shear angle in counter-clockwise direction in degrees)
                    zoom_range=0.2,             # float, it defines this range for zooming: [lower, upper] = [1-zoom_range, 1+zoom_range]
                    horizontal_flip=True,       # bool, randomly flip inputs horizontally
                    fill_mode="nearest",        # One of {"constant", "nearest", "reflect" or "wrap"}
                    )

# note that the validation data should not be augmented!
validation_datagen = ImageDataGenerator(
                    rescale=1./255              # we multiply the data by the value provided (after applying all other transformations)
                    )

train_generator = train_datagen.flow_from_directory(
                    train_dir,
                    target_size=(150, 150),     # resizes all images to (150, 150), they are still color images
                    batch_size=20,
                    class_mode="binary",        # becuase you use binary_crossentropy, you need binary labels
                    )

validation_generator = validation_datagen.flow_from_directory(
                    validation_dir,
                    target_size=(150, 150),     # resizes all images to (150, 150), they are still color images
                    batch_size=20,
                    class_mode="binary",        # becuase you use binary_crossentropy, you need binary labels
                    )

model.compile(
            loss="binary_crossentropy",
            optimizer=optimizers.RMSprop(lr=2e-5),
            metrics=["acc"])
            
# add a callback to save the model during training
callbacks = [
        ModelCheckpoint(
            filepath="dogs_vs_cats_v3_callback.h5",
            monitor="val_loss",                         # these two arguments mean you won't overwrite the
            save_best_only=True,                        # model file unless val_loss has improved, which allows
        )                                               # you to keep the best model seen during training
    ]

history = model.fit_generator(
            train_generator,
            steps_per_epoch=100,                # remember, batch_size is specified in flow_from_directory()
            epochs=30,
            validation_data=validation_generator,
            validation_steps=50,
            callbacks=callbacks
            )


# save model
print("saving model...")
model.save("dogs_vs_cats_v3.h5")
print("OK")
