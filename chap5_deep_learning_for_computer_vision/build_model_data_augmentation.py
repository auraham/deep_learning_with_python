# build_model_data_augmentation.py
from __future__ import print_function
import numpy as np
from keras import layers
from keras import models
from keras import optimizers
from keras.preprocessing.image import ImageDataGenerator
from keras.preprocessing import image
import matplotlib.pyplot as plt
import os

if __name__ == "__main__":
    
    input_h = 150   # image height
    input_w = 150   # image width
    input_c = 3     # number of channels
    save_model = True
    
    """
    train (cats):       /media/data/dogs_vs_cats_small/train/cats
    train (dogs):       /media/data/dogs_vs_cats_small/train/dogs
    validation (cats):  /media/data/dogs_vs_cats_small/validation/cats
    validation (dogs):  /media/data/dogs_vs_cats_small/validation/dogs
    test (cats):        /media/data/dogs_vs_cats_small/test/cats
    test (dogs):        /media/data/dogs_vs_cats_small/test/dogs
    """
    
    # --- data preprocessing with data augmentation ---
    datagen = ImageDataGenerator(rotation_range=40,         # value in degrees (0-180), a range within which to randomly rotate pictures
                                width_shift_range=0.2,      # ranges (as a fraction of total width or height)
                                height_shift_range=0.2,     # within which to randomly translate pictures vertically or horizontally
                                shear_range=0.2,            # randomly applying shearing transformations
                                zoom_range=0.2,             # randomly zooming inside pictures
                                horizontal_flip=True,       # randomly flipping half the images horizontally
                                fill_mode="nearest"         # strategy used for filling in new created pixels, which can appear after a rotation or a width/height shift
                                )
                                
    # --- display some randomly augmented training images ---
    train_cats_dir = "/media/data/dogs_vs_cats_small/train/cats"
    fnames = [ os.path.join(train_cats_dir, fname) for fname in os.listdir(train_cats_dir)]
    
    img_path = fnames[3]
    
    img = image.load_img(img_path, target_size=(input_h, input_w))

    x = image.img_to_array(img)
    x = x.reshape((1, ) + x.shape)
    
    i = 0
    for batch in datagen.flow(x, batch_size=1):
        
        plt.figure(i)
        imgplot = plt.imshow(image.array_to_img(batch[0]))
        i += 1
        
        if i % 4 == 0:
            break
            
    plt.show()
        
