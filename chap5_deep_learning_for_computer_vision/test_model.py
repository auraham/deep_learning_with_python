# test_model.py
from __future__ import print_function
import numpy as np
import matplotlib.pyplot as plt
from keras.preprocessing import image
from keras.preprocessing.image import ImageDataGenerator
from keras.models import load_model
import os

def load_set(fnames):
    
    images = []
    
    for fname in fnames:
        
        # format the input image using the format expected by the model,
        # ie (float, range [0, 1])
        img = image.load_img(fname)
        x = image.img_to_array(img) * 1./255
        
        images.append(x)
        
    return images


if __name__ == "__main__":
    
    input_h = 150       # height
    input_w = 150       # width
    show_images = False

    # --- paths ----
    # train (cats):       /media/data/dogs_vs_cats_small/train/cats
    # train (dogs):       /media/data/dogs_vs_cats_small/train/dogs
    # validation (cats):  /media/data/dogs_vs_cats_small/validation/cats
    # validation (dogs):  /media/data/dogs_vs_cats_small/validation/dogs
    # test (cats):        /media/data/dogs_vs_cats_small/test/cats
    # test (dogs):        /media/data/dogs_vs_cats_small/test/dogs

    # --- display images (they are not preprocessed yet) ---
    test_dir_cats = "/media/data/dogs_vs_cats_small/test/cats"
    test_dir_dogs = "/media/data/dogs_vs_cats_small/test/dogs"

    fnames_dogs = [os.path.join(test_dir_dogs, fname) for fname in os.listdir(test_dir_dogs)]
    fnames_cats = [os.path.join(test_dir_cats, fname) for fname in os.listdir(test_dir_cats)]

    if show_images:

        # convert PIL to np.array (cast to np.int in the range [0, 255])
        img_path = fnames_dogs[4]
        img = image.load_img(img_path)
        x = image.img_to_array(img).astype(int)
        plt.figure()
        plt.imshow(x)
        
        # convert PIL to np.array (as np.float32 in the range [0, 1])
        img_path = fnames_dogs[5]
        img = image.load_img(img_path)
        x = image.img_to_array(img) * 1./255
        plt.figure()
        plt.imshow(x)
        
        plt.show()
    
    # --- create data generator to preprocess input images ---
    test_dir = "/media/data/dogs_vs_cats_small/test"
    test_datagen = ImageDataGenerator(rescale=1./255)
    test_generator = test_datagen.flow_from_directory(test_dir, 
                                                    target_size=(input_h, input_w),
                                                    batch_size=32,
                                                    class_mode="binary"
                                                    )
    # --- select images from test split ---
    set_fnames_dogs = fnames_dogs[:9]
    set_dogs = load_set(set_fnames_dogs)
    
    # --- load model ----
    model = load_model("dogs_vs_cats_small_v1.h5")
    
    # --- test model ----
    #for batch in test_generator.flow()
    #model.predict(set_dogs)
    
    
    # 1. did not work
    """
    for batch in test_generator:
        
        pred = model.predict(batch)
        break
    """
    
    # it works
    #pred = model.predict(test_generator)
        
    # it works
    # get the first batch
    for batch, labels in test_generator:
        
        # get predictions
        pred = model.predict(batch).flatten()
        
        # change prediction to int [0 or 1]
        pred_int = np.zeros_like(labels)
        to_keep = pred > 0.5
        
        # cast
        pred_int[to_keep]  = 1  # class 1
        pred_int[~to_keep] = 0  # class 0
        
        # missclassified examples
        error = (pred_int != labels).sum()
        n = len(labels)
        print("error: %d/%d" % (error, n))

        break
    
    

