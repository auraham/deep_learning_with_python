# build_model_v2_feature_extraction.py
import os
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.pyplot import rcParams
from keras import models
from keras import layers
from keras import optimizers
from keras.preprocessing.image import ImageDataGenerator
from keras.applications import VGG16
from keras.callbacks import ModelCheckpoint

def load_rcparams(figsize=None):
    """
    Load a custom rcParams dictionary
    """
    
    rcParams['axes.titlesize']  = 14            # title
    rcParams['axes.labelsize']  = 10            # $f_i$ labels
    rcParams['xtick.color']     = "#474747"     # ticks gray color
    rcParams['ytick.color']     = "#474747"     # ticks gray color
    rcParams['xtick.labelsize'] = 10            # ticks size
    rcParams['ytick.labelsize'] = 10            # ticks size
    rcParams['legend.fontsize'] = 12            # legend
    rcParams['legend.fontsize'] = 12            # legend

    if isinstance(figsize, tuple):
        rcParams['figure.figsize'] = figsize    

def extract_features(conv_base, directory, sample_count):
    """
    Return feature vectors from images
    
        features = conv_base.predict(inputs_batch)
    """
    
    datagen = ImageDataGenerator(rescale=1./255)
    batch_size = 20
    
    features = np.zeros((sample_count, 4, 4, 512))  # por que este shape
    labels = np.zeros((sample_count, ))
    
    generator = datagen.flow_from_directory(
                        directory,
                        target_size=(150, 150),
                        batch_size=batch_size,
                        class_mode="binary")
                        
    i = 0
    for inputs_batch, labels_batch in generator:        # que forma tiene cada batch
                                                        # inputs_batchs is a numpy array of PIL->numpy matrices?
                                                        # labels_batchs is a numpy array with the real labels of the input images?
        # compute feature vectors
        features_batch = conv_base.predict(inputs_batch)
        
        # save feature vectors
        a = i * batch_size
        b = (i+1) * batch_size
        features[a:b] = features_batch.copy()
        labels[a:b] = labels_batch.copy()
        
        i+=1
        
        if i * batch_size >= sample_count:
            # stop feature extraction after loading the max amount of images (sample_count)
            # otherwise, some images (ie, feature vectors) will be duplicated
            break
            
    return features, labels
            
        
        
        

if __name__ == "__main__":
    
    base_dir = "/media/data/dogs_vs_cats_small"
    train_dir = os.path.join(base_dir, "train")
    validation_dir = os.path.join(base_dir, "validation")
    test_dir = os.path.join(base_dir, "test")
    
    extract_features = False
    
    if extract_features:
    
        # Listing 5.16
        # Instantiate the VGG16 model
        conv_base = VGG16(
                    weights="imagenet",
                    include_top=False,          # include or not the densely connected classifier on top of the network
                    input_shape=(150, 150, 3)   # shape of the image tensors that you will feed to the network
                        )
        
        # check the model
        conv_base.summary()
        
        # Listing 5.17
        # Extracting features using the pretrained convolutional base
        
        
        
        # extract features
        train_features, train_labels = extract_features(conv_base, train_dir, 2000)
        validation_features, validation_labels = extract_features(conv_base, validation_dir, 1000)
        test_features, test_labels = extract_features(conv_base, test_dir, 1000)
        
        # reshape features
        train_features = np.reshape(train_features, (2000, 4*4*512))
        validation_features = np.reshape(validation_features, (1000, 4*4*512))
        test_features = np.reshape(test_features, (1000, 4*4*512))
        
        # save features
        np.savetxt(os.path.join(base_dir, "train_features.txt"), train_features)
        np.savetxt(os.path.join(base_dir, "validation_features.txt"), validation_features)
        np.savetxt(os.path.join(base_dir, "test_features.txt"), test_features)
    
        # save labels
        np.savetxt(os.path.join(base_dir, "train_labels.txt"), train_labels)
        np.savetxt(os.path.join(base_dir, "validation_labels.txt"), validation_labels)
        np.savetxt(os.path.join(base_dir, "test_labels.txt"), test_labels)
    
    else:
        
        # this will take a few seconds (requires a lot of memory, about 3.5 Gb)
        print("loading features...")
        
        # load features from file
        train_features = np.genfromtxt(os.path.join(base_dir, "train_features.txt"))
        train_labels = np.genfromtxt(os.path.join(base_dir, "train_labels.txt"))
        
        validation_features = np.genfromtxt(os.path.join(base_dir, "validation_features.txt"))
        validation_labels = np.genfromtxt(os.path.join(base_dir, "validation_labels.txt"))
        
        test_features = np.genfromtxt(os.path.join(base_dir, "test_features.txt"))
        test_labels = np.genfromtxt(os.path.join(base_dir, "test_labels.txt"))
       
        print("OK")
        
        # Listing 5.18
        # Defining and training the densely connected classifier
        
        model = models.Sequential()
        model.add(layers.Dense(256, activation="relu", input_dim=4*4*512))      # porque el input_dim no es una tupla?
        model.add(layers.Dropout(0.5))
        model.add(layers.Dense(1, activation="sigmoid"))
        
        model.compile(
                    optimizer=optimizers.RMSprop(lr=2e-5),
                    loss="binary_crossentropy",
                    metrics=["acc"]
                    )
                    
        # add a callback to save the model during training
        callbacks = [
                ModelCheckpoint(
                    filepath="dogs_vs_cats_v2_callback.h5",
                    monitor="val_loss",                         # these two arguments mean you won't overwrite the
                    save_best_only=True,                        # model file unless val_loss has improved, which allows
                )                                               # you to keep the best model seen during training
            ]
            
        # each epoch takes 3s in my computer
        # Epoch 30/30
        # 2000/2000 [==============================] - 3s 2ms/step - loss: 0.0904 - acc: 0.9720 - val_loss: 0.2458 - val_acc: 0.9010
        history = model.fit(train_features, train_labels,
                            epochs=30,
                            batch_size=20,
                            validation_data=(validation_features, validation_labels),
                            callbacks=callbacks
                            )
        
        # save model
        model.save("dogs_vs_cats_v2.h5")
        
        # use this command to show the metrics (required to know the output of model.evaluate)
        print(model.metrics_names)
        
        # evaluate model
        test_loss, test_acc = model.evaluate(test_features, test_labels, batch_size=20)
        print("test acc: %.4f" % test_acc)          # test acc: 0.8910

        # Listing 5.19
        # Plotting the results
        acc = history.history["acc"]
        val_acc = history.history["val_acc"]
        loss = history.history["loss"]
        val_loss = history.history["val_loss"]
        
        epochs = range(1, len(acc)+1)
        
        load_rcparams((6, 8))

        fig = plt.figure()
        ax_acc = fig.add_subplot(211)
        ax_loss = fig.add_subplot(212)

        ax_acc.plot(epochs, acc, "o", color="#E4AFC8", ls="-", label="Training")
        ax_acc.plot(epochs, val_acc, "o", color="#A91458", ls="-", label="Validation")
        ax_acc.set_title("Accuracy")
        ax_acc.legend()

        ax_loss.plot(epochs, loss, "o", color="#E4AFC8", ls="-", label="Training")
        ax_loss.plot(epochs, val_loss, "o", color="#A91458", ls="-", label="Validation")
        ax_loss.set_title("Loss")
        ax_loss.legend()

        # save plot
        output = "build_model_v2_feature_extraction.png"
        fig.savefig(output, dpi=300)
        print(output)

        plt.show()


