# classify_and_image_data_generator.py
import numpy as np
import matplotlib.pyplot as plt
from keras.preprocessing import image
from keras.preprocessing.image import ImageDataGenerator
from keras.models import load_model
import os

def display_batch(batch, label="", limit=10):
    """
    Plots the images in the batch
    """
    
    for i, img in enumerate(batch):
        
        plt.figure()
        plt.imshow(img)
        title = "class: %s id: %d" % (label, i)
        plt.title(title)
        
        if (i+1) == limit:
            break

def create_batch(fnames):
    """
    Load images from a list of file paths.
    The images are preprocessed to match the shape and size expected by the model:
      - The images are rescaled in the range [0, 1]
      - The size is (150, 150)
    The output is a tensor of shape (n_samples, height, width, n_channels)
    """
    
    images = []
    
    for fname in fnames:
        
        # convert PIL to np.array (as np.float32 in the range [0, 1])
        img = image.load_img(fname,
                            color_mode="rgb",
                            target_size=(150, 150))
        x = image.img_to_array(img) * 1./255

        images.append(x)
        
    return np.array(images)
    

if __name__ == "__main__":
    
    input_h = 150       # image height
    input_w = 150       # image width
    show_images = True

    # --- paths ----
    # train (cats):       /media/data/dogs_vs_cats_small/train/cats
    # train (dogs):       /media/data/dogs_vs_cats_small/train/dogs
    # validation (cats):  /media/data/dogs_vs_cats_small/validation/cats
    # validation (dogs):  /media/data/dogs_vs_cats_small/validation/dogs
    # test (cats):        /media/data/dogs_vs_cats_small/test/cats
    # test (dogs):        /media/data/dogs_vs_cats_small/test/dogs

    # --- display images (they are not preprocessed yet) ---
    test_dir_cats = "/media/data/dogs_vs_cats_small/test/cats"
    test_dir_dogs = "/media/data/dogs_vs_cats_small/test/dogs"

    fnames_dogs = [os.path.join(test_dir_dogs, fname) for fname in os.listdir(test_dir_dogs)]
    fnames_cats = [os.path.join(test_dir_cats, fname) for fname in os.listdir(test_dir_cats)]

    if show_images:

        # convert PIL to np.array (cast to np.int in the range [0, 255])
        img_path = fnames_dogs[4]
        img = image.load_img(img_path)
        x = image.img_to_array(img).astype(int)
        plt.figure()
        plt.imshow(x)

        # convert PIL to np.array (as np.float32 in the range [0, 1])
        img_path = fnames_dogs[5]
        img = image.load_img(img_path)
        x = image.img_to_array(img) * 1./255
        plt.figure()
        plt.imshow(x)
        
        # convert PIL to np.array (cast to np.int in the range [0, 255])
        img_path = fnames_cats[4]
        img = image.load_img(img_path)
        x = image.img_to_array(img).astype(int)
        plt.figure()
        plt.imshow(x)

        # convert PIL to np.array (as np.float32 in the range [0, 1])
        img_path = fnames_cats[5]
        img = image.load_img(img_path)
        x = image.img_to_array(img) * 1./255
        plt.figure()
        plt.imshow(x)

        plt.show()


    fnames_dogs = [os.path.join(test_dir_dogs, fname) for fname in os.listdir(test_dir_dogs)]
    fnames_cats = [os.path.join(test_dir_cats, fname) for fname in os.listdir(test_dir_cats)]

    # create the batch for dogs and cats
    batch_dogs = create_batch(fnames_dogs[:32])
    batch_cats = create_batch(fnames_cats[:32])


    if show_images:
        display_batch(batch_dogs, "dogs", limit=5)
        display_batch(batch_cats, "cats", limit=5)
        
    # ---
    # generator: it is only needed for getting the mapping of the class labels (from int id to str label)
    train_datagen = ImageDataGenerator(rescale=1./255)      # rescale images by 1/255 
    train_dir = "/media/data/dogs_vs_cats_small/train"
    train_generator = train_datagen.flow_from_directory(train_dir,                          # path of images
                                                        target_size=(input_h, input_w),     # image size
                                                        batch_size=20,                      # number of images per batch
                                                        class_mode="binary"                 # because you use binary_crossentropy, you need binary labels
                                                        )
    # mapping: {'cats': 0, 'dogs': 1}
    class_indices = train_generator.class_indices
    # ---
    
    # load model
    model = load_model("dogs_vs_cats_small_v1.h5")

    # prediction
    pred_dogs = model.predict(batch_dogs)
    pred_cats = model.predict(batch_cats)
    
    # cast to int
    pred_dogs_classes = (pred_dogs > 0.5).astype("int32").flatten()
    pred_cats_classes = (pred_cats > 0.5).astype("int32").flatten()


    # create image data generator to preprocess input images
    test_dir = "/media/data/dogs_vs_cats_small/test"              # we assume this path contains a directory for each class
    test_datagen = ImageDataGenerator(rescale=1./255)
    test_generator = test_datagen.flow_from_directory(test_dir,                         # path of test images
                                                      target_size=(input_h, input_w),   # shape expected by the model
                                                      color_mode="rgb",
                                                      class_mode="binary",
                                                      batch_size=32,
                                                      shuffle=False,
                                                      seed=42,
                                                     )

    # this line is needed to yield the batches from the top
    test_generator.reset()

    for batch, labels in test_generator:
        
        # get predictions
        pred = model.predict(batch)
        
        # debug
        #print(labels)
        #print(pred)
        
        # cast to int
        pred = (pred > 0.5).astype("int32").flatten()
        
        # measure error (missclassified samples)
        error = (pred != labels).sum()
        print("missclassified:", error)
        
        display_batch(batch, "cats?", limit=5)
        
        # stop classification
        break
