# listing 6.1
# world-level one-hot encoding (toy example)
import numpy as np

# Initial data: one entry per sample (in this example, a sample
# is a sentence, but it could be an entire document)
samples = ["The cat sat on the mat.", "The dog ate my homework."]


token_index = {}
for sample in samples:
    for word in sample.split():
        
        # preprocess word
        pword = word.lower()
        
        if pword not in token_index:
            # Assign a unique index to each unique word.
            # Note that you don't attribute index 0 to anything
            token_index[pword] = len(token_index) + 1

print("Found %s unique tokens." % len(token_index))

max_length = 10  # columns
results = np.zeros(shape=(len(samples),
                          max_length,
                          max(token_index.values()) + 1))

for i, sample in enumerate(samples):
    for j, word in list(enumerate(sample.split()))[:max_length]:
        
        # preprocess word
        pword = word.lower()
        
        index = token_index.get(pword)
        results[i, j, index] = 1
